---
title: Contribute
description: Contribute to Hacker Lab documentation.
extends: _layouts.documentation
section: content
---
# Contribute

## Introduction

You are here to help Hacker Lab Learn? Awesome! Feel welcome and read the following sections in order to know how to ask questions and how to work on something.

Following these guidelines helps to communicate that you respect the time of the developers managing and developing this open source project. In return, they should reciprocate that respect in addressing your issue, assessing changes, and helping you finalize your pull requests.

## Contributions we are looking for

Hacker Lab Learn is an open source project and we love to receive contributions from our community — you! There are many ways to contribute, from writing tutorials or blog posts, improving the documentation, submitting bug reports and feature requests or writing code which can be incorporated into Hacker Lab Learn itself.

Feel free to submit any change, big or small:

* Spelling / grammar fixes
* Typo correction, white space and formatting changes
* Comment clean up
* Bug fixes that change default return values or error codes stored in constants
* Adding logging messages or debugging output
* Changes to ‘metadata’ files like Gemfile, .gitignore, build scripts, etc.
* Moving source files from one directory or package to another

Please, don't use the issue tracker for [support questions]. Check whether the discord server can help with your issue.

## Ground Rules

Responsibilities

* Ensure cross-browser compatibility for every change that's accepted.
* Create issues for any major changes and enhancements that you wish to make. Discuss things transparently and get community feedback.
* Keep feature versions as small as possible, preferably one new feature per version.
* Be welcoming to newcomers and encourage diverse new contributors from all backgrounds. See the [Code of Conduct](https://gitlab.com/the-hacker-lab/learn/blob/master/CODE_OF_CONDUCT.md).

## Your First Contribution

Unsure where to begin contributing to Hacker Lab Learn? You can start by looking through these beginner and help-wanted issues:

* **Beginner issues** - issues which should only require a few lines of code, and a test or two.
* **Help wanted issues** - issues which should be a bit more involved than beginner issues.

Both issue lists are sorted by total number of comments. While not perfect, number of comments is a reasonable proxy for impact a given change will have.

Working on your first Pull Request? You can learn [Project Forking Workflow](https://docs.gitlab.com/ee/workflow/forking_workflow.html).

At this point, you're ready to make your changes! Feel free to ask for help; everyone is a beginner at first :smile_cat:

>Tip: If a maintainer asks you to "rebase" your PR, they're saying that a lot of code has changed, and that you need to update your branch so it's easier to merge.

## Getting started

1. Create your own fork of the code
2. Do the changes in your fork
3. Submit Merge Request

[Reference: [Project Forking Workflow](https://docs.gitlab.com/ee/workflow/forking_workflow.html)]

## How to report a bug

If you find a security vulnerability, do NOT open an issue. Email wagena@thehackerlab.co instead.

In order to determine whether you are dealing with a security issue, ask yourself these two questions:

* Can I access something that's not mine, or something I shouldn't have access to?
* Can I disable something for other people?

If the answer to either of those two questions are "yes", then you're probably dealing with a security issue. 
Note that even if you answer "no" to both questions, you may still be dealing with a security issue, so if you're unsure, just email us at wagena@thehackerlab.co.

When filing an issue, make sure to answer these five questions:

1. What browser and version are you using?
2. What operating system and processor architecture are you using?
3. What did you do?
4. What did you expect to see?
5. What did you see instead?

## How to suggest a feature or enhancement

If you find yourself wishing for an explanation of a concept or tool that doesn't exist in Hacker Lab, you are probably not alone. 
There are bound to be others out there with similar needs. 
Many of the docs that Hacker Lab has today have been added because our users saw the need. 

Open an issue on our issues list on GitHub which describes the feature you would like to see, why you need it, and how it should work.

## Code review process

The core team looks at Pull Requests on a regular basis and merge requests are merged as soon as possible

After feedback has been given we expect responses within two weeks. After two weeks we may close the pull request if it isn't showing any activity.