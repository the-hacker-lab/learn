---
title: 5 of Hearts
description: Metasploitable 3 Ubuntu - 5 of Hearts
extends: _layouts.documentation
section: content
---

# 5 of Hearts

1\. First, get logged into your Kali Linux instance. Checkout the [Quickstart Guide](/walkthroughs/metasploitable-3-ubuntu/quickstart/) if you're not sure how to do this. 

Next, let's start off by running the same `nmap` scan we ran in the first step of the [Ace of Clubs](/walkthroughs/metasploitable-3-ubuntu/ace-of-clubs) walkthrough guide.

```bash
# replace 165.22.171.133 with the IP of your target machine
$ nmap -p 1-65535 165.22.171.133
```

>Hint: Refer to the [Ace of Clubs](/walkthroughs/metasploitable-3-ubuntu/ace-of-clubs) walkthrough guide if you need a refresher on what we're doing with this `nmap` command.

2\. Navigate to port 80 in your browser to see an open directory

<img src="/assets/images/walkthroughs/metasploitable-3-ubuntu/5-of-hearts/1-port-80.PNG">

3\. Explore the links until you a drupal instance.

<img src="/assets/images/walkthroughs/metasploitable-3-ubuntu/5-of-hearts/2-drupal.PNG">

4\. One of the tabs has an image of a heart.

<img src="/assets/images/walkthroughs/metasploitable-3-ubuntu/5-of-hearts/3-high-fives.PNG">

5\. this isn't the flag, but if you right click the image and show the EXIF data, you'll see the exif data contains a string starting with `data:image/png;base64,iVBORw0K...`
If you've done web development you may recognize `iVBORw0K` as an image that is base64 encoded. Usually used to embed an image straight into html.

<img src="/assets/images/walkthroughs/metasploitable-3-ubuntu/5-of-hearts/4-view-image-info.PNG">

6\. This means there is an image embedded within the original image's EXIF data. Let's download the original image and inspect the hidden image.

```bash
# download image using curl
# replace 165.227.59.82 with IP of your target machine
$ curl http://165.227.59.82/drupal/sites/default/files/styles/large/public/field/image/5_of_hearts.png > image.png
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  496k  100  496k    0     0  3356k      0 --:--:-- --:--:-- --:--:-- 3356k
```

7\. In order to decode the exif, we're going to pipe together the output of several command line tools:

* `exiftool image.png` - prints a table of exif data for the original image
* `grep hearts` - filter results to the line that contains the string "hearts"
* `awk '{print $6}'` - print the word of the line returned from grep. Which in this case is the base64 encoded string.
* `base64 -d > five_of_hearts.png` - decode the string into five_of_hearts.png

```bash
# pipe the output of each command as the input into the next
$ exiftool image.png | grep hearts | awk '{print $6}' | base64 -d > five_of_hearts.png
```

>Hint: linux pipes allow you to send the output of one command to the input of the next command using the `|` operator.
>Checkout the [tutorial](https://ryanstutorials.net/linuxtutorial/piping.php) to learn more.

8\. Download the new image to your local computer if you're running kali from a vps

```bash
# run this from your local computer
# replace 167.99.160.109 with the IP of your Kali Linux instance
$ scp -P 2222 hacker-lab@167.99.160.109:/home/hacker-lab/five_of_hearts.png ./

The authenticity of host '167.99.160.109 (167.99.160.109)' can't be established.
ECDSA key fingerprint is SHA256:sGnIbDcLbUCdWnjvkbKs+SbADQhCiOM8BVU3g4jDZig.
Are you sure you want to continue connecting (yes/no)? yes
Failed to add the host to the list of known hosts (/home/wagena/.ssh/known_hosts).
Enter passphrase for key '../hacker-lab':
five_of_hearts.png                                                           100%  456KB 330.8KB/s   00:01
```

>Hint: `scp` is a command line tool that allows you to securely copy files between local and remote hosts.
>Checkout our [essentials documentation](/essentials/scp) to learn more.

9\. And you've got another flag!

<img src="/assets/images/walkthroughs/metasploitable-3-ubuntu/5-of-hearts/five_of_hearts.png">
