---
title: Metasploitable 3 Ubuntu Quickstart
description: Metasploitable 3 Ubuntu Quickstart
extends: _layouts.documentation
section: content
---

# Metasploitable 3 Ubuntu Quickstart Guide

> Metasploitable3 is a VM that is built from the ground up with a large amount of security vulnerabilities. It is intended to be used as a target for testing exploits with metasploit.

\- Metasploitable 3 Team

Metasploitable 3 Ubuntu contains 13 flags (posing as images of playing cards) for you to find and capture. 
There are also plenty of vulnerable services, backdoors, and un-patched software for you to experiment with.
See [Metasploitable 3 Windows](/walkthroughs/metasploitable-3-windows/quickstart) if you want to practice hacking a windows based machine.

---

## Prerequisites

You will need these servers setup before we start:

* Metasploitable 3 Ubuntu instance
* Kali Linux instance (optional)

You can setup Metasploitable locally using vagrant by following the [official documentation](https://github.com/rapid7/metasploitable3).
Alternatively you can use a paid service like [thehackerlab.co](https://thehackerlab.co) to set this up for you.

---

## Login to Kali Linux (Optional)

1\. If you've chosen to setup the optional Kali Linux instance, [ssh](/essentials/ssh) into your Kali Linux server now.

```bash
# replace 178.128.77.75 with the IP address of your Kali Linux instance
$ ssh -p 2222 hacker-lab@178.128.77.75
```

[thehackerlab.co](https://thehackerlab.co) sets up the ssh service on port `2222` with access for the user `hacker-lab` using the ssh keys provided at setup by default.
Replace the user and port number with the correct values if you set it up manually.

---

## Start CTF challenges

1\. Once you have your lab setup, checkout our walkthrough of the [Ace of Clubs](https://learn.thehackerlab.co/walkthroughs/metasploitable-3-ubuntu/ace-of-clubs/) to capture your first flag.

---

## More Resources

* [Rapid7 Download](https://information.rapid7.com/download-metasploitable-2017.html)
* [Github](https://github.com/rapid7/metasploitable3)
* [Vagrant](https://app.vagrantup.com/rapid7/)
* [Wiki](https://github.com/rapid7/metasploitable3/wiki)