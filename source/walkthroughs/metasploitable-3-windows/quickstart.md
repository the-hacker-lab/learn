---
title: Metasploitable 3 Windows Quickstart
description: Metasploitable 3 Windows Quickstart
extends: _layouts.documentation
section: content
---

# Metasploitable 3 Windows 

> Metasploitable3 is a VM that is built from the ground up with a large amount of security vulnerabilities. It is intended to be used as a target for testing exploits with metasploit.

\- Metasploitable 3 Team

Metasploitable 3 Windows contains 13 flags (posing as images of playing cards) for you to find and capture. 
There are also plenty of vulnerable services, backdoors, and un-patched software for you to experiment with.
See [Metasploitable 3 Ubuntu](/walkthroughs/metasploitable-3-windows) if you want to practice hacking a linux based machine.

## Quickstart

This quickstart guide will walk you through setting up the following components in a personal, private, cloud hosted lab environment using [Hacker Lab](https://thehackerlab.co):

* Metasploitable 3 Windows instance
* Kali Linux instance (optional)

You may also setup Metasploitable 3 locally using vagrant by following the [official documentation](https://github.com/rapid7/metasploitable3).

## Setup Lab

1\. Look for the **Metasploitable 3 Windows** Lab at [thehackerlab.co](thehackerlab.co) and click create.

2\. The "metasploitable 3 Windows" Lab has some settings for you to configure:

* **IP Address** - The public IP Address you will access this lab from. We will allow access to this lab from this IP Address only.
* **Time to Live** - How many hours do you want this lab living on the internet. 
* **Include Kali Instance (optional)** - A Kali Linux instance is required for this lab. If you don't have your own, check this box and we'll create one for you to use with this lab. [Read more about Kali Linux.](/docs/navigation)
* **SSH Key** - If including the optional Kali instance, you'll need to submit your public SSH key so you can SSH into the Kali instance we create. [Read more about SSH Keys.](/docs/navigation)
 
<img src="/assets/images/walkthroughs/metasploitable-3-windows/quickstart/1-create-lab.PNG">

3\. Click create. This may take 5-10 minutes to set everything up. 

## Login to Kali Linux

1\. When the lab setup is completed, you'll see some information on your home screen.

* Name - name of the lab, in the case "Metasploitable 3 Windows"
* Kali Linux - if you chose to include a Kali instance, this is the IP address you'll be able to login at.
* Access IP - this is the IP Address you'll be able to access the lab from.
* Location - this is the IP Address that the lab is located at.

<img src="/assets/images/walkthroughs/metasploitable-3-windows/quickstart/2-find-lab-location.PNG"> 

2\. SSH into your Kali Linux instance
```bash
ssh -p 2222 hacker-lab@206.189.218.75
```

3\. Visiting the open http ports through a browser, you'll see that http://75.85.140.158:8022 redirects to a ManageEngine installation

<img src="/assets/images/walkthroughs/metasploitable-3-windows/quickstart/3-manage-engine.PNG">

## More Resources

* [Rapid7 Download](https://information.rapid7.com/download-metasploitable-2017.html)
* [Github](https://github.com/rapid7/metasploitable3)
* [Vagrant](https://app.vagrantup.com/rapid7/)
* [Wiki](https://github.com/rapid7/metasploitable3/wiki)





