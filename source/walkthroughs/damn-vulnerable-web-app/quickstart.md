---
title: Damn Vulnerable Web App Quickstart
description: Damn Vulnerable Web App Quickstart
extends: _layouts.documentation
section: content
---

# Damn Vulnerable Web App (DVWA)

> Damn Vulnerable Web App (DVWA) is a PHP/MySQL web application that is damn vulnerable. Its main goals are to be an aid for security professionals to test their skills and tools in a legal environment, help web developers better understand the processes of securing web applications and aid teachers/students to teach/learn web application security in a class room environment.

\- The DVWA Team

DVWA 14 contains eleven lesson modules. 
Each lesson has a common web vulnerability you can exploit, with links to more information about that vulnerability.
The lessons are all set to to a low difficulty by default. But can be increased to medium, high, or impossible levels as you progress.

---

## Quickstart

The following quickstart guide will walk you through setting up the following components in a personal, private, cloud hosted lab environment using [Hacker Lab](https://thehackerlab.co):

* DVWA instance
* Kali Linux instance (optional)

You may also setup DVWA locally using docker by following the [official documentation](https://github.com/ethicalhack3r/DVWA).

---

## Prerequisites

You will need these servers setup before we start:

* Damn Vulnerable Web App (DVWA) instance
* Kali Linux instance (optional)

You can setup DVWA locally using docker, or as a standalone application by following the [official documentation](https://github.com/ethicalhack3r/DVWA).
Alternatively you can use a paid service like [thehackerlab.co](https://thehackerlab.co) to set this up for you.

---

## Access DVWA Lessons

1\. You can access your DVWA instance by navigating to `138.68.7.66 ` in your browser.
Replace `138.68.7.66 ` with the **Location** IP address of your lab.

Note: the first time you login, you'll have to click on the **Reset DB** button to generate the lessons.

<img src="/assets/images/walkthroughs/damn-vulnerable-web-app/quickstart/1-login.PNG"> 

2\. Login using the default credentials: `admin/password`

<img src="/assets/images/walkthroughs/damn-vulnerable-web-app/quickstart/2-home.PNG">

3\. Each module listed in the navigation menu on the left includes lessons in incremental difficulty that teach you about the listed vulnerability.
For example click on the **Brute Force** menu item and attempt to brute force the example login page.

<img src="/assets/images/walkthroughs/damn-vulnerable-web-app/quickstart/3-brute-force.PNG">

---

## Login to Kali Linux (Optional)

1\. If you've chosen to setup the optional Kali Linux instance, [ssh](/essentials/ssh) into your Kali Linux server now.

```bash
# replace 178.128.77.75 with the IP address of your Kali Linux instance
ssh -p 2222 hacker-lab@104.248.66.22
```

[thehackerlab.co](https://thehackerlab.co) sets up the ssh service on port `2222` with access for the user `hacker-lab` using the ssh keys provided at setup by default.
Replace the user and port number with the correct values if you set it up manually.

---

## More Resources

* [Damn Vulnerable Web App (DVWA)](http://www.dvwa.co.uk/)
* [Github](https://github.com/ethicalhack3r/DVWA)
* [Docker](https://hub.docker.com/r/vulnerables/web-dvwa/)
