---
title: CSRF
description: Damn Vulnerable Web App - Cross Site Request Forgery (CSRF)
extends: _layouts.documentation
section: content
---

# Cross Site Request Forgery (CSRF)

1\. Navigate to the command injection lesson and open up the **Browser Developer Tools** with the keyboard shortcut `CTRL + SHIFT + I`.
Then open the **Network** tab so we can view the network request made when we submit the form.

>Hint: The developer console is part of a set of developer tools built right into most popular web browsers.
>We can use it to inspect what data the web page is sending back to the browser.
>You can learn more about what the developer tools can be used for [here](/essentials/browser-developer-tools).

<img src="/assets/images/walkthroughs/damn-vulnerable-web-app/cross-site-request-forgery/2-network-tab.PNG">

3\. Now let's fill out the form with a random guess and submit the form.
The purpose of this test submission is so we can inspect what the form sends back to the server using the **Firefox Developer Tools**.

After submitting the form check out the network tab and look through the different requests made. 
One of the requests is to:

`http://165.227.3.2/vulnerabilities/csrf/?password_new=test1&password_conf=test2&Change=Change`

4\. This means we can make password changes for whoever is logged in at the moment by just tricking the browser into making this request (replace `165.227.3.2` with the IP address of your DVWA instance):

`http://165.227.3.2/vulnerabilities/csrf/?password_new=myNewPassword&password_conf=myNewPassword&Change=Change`

When a user navigates to this URL, it will change the password of whatever user is logged in at the moment.

5\. So here is the scenario: Imagine you disguise the URL above like this: [myfakewebsite.com](http://165.227.3.2/vulnerabilities/csrf/?password_new=myNewPassword&password_conf=myNewPassword&Change=Change) (replace `165.227.3.2` with the IP address of your DVWA instance).

Send that disguised link to someone else who you know is using this site and they will accidentally change their own password when they click the link. 
Now you know their password!

Imagine if we followed these steps for an insecure bank website, but instead of changing the password, we make a money transfer.
