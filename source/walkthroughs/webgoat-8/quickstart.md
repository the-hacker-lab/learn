---
title: WebGoat 8 Quickstart
description: WebGoat 8 Quickstart
extends: _layouts.documentation
section: content
---

# WebGoat 8 Quickstart Guide

> WebGoat is a deliberately insecure application that allows interested developers just like you to test vulnerabilities commonly found in Java-based applications that use common and popular open source components.

\- The WebGoat Team

[WebGoat 8](https://github.com/WebGoat/WebGoat) contains eleven lesson modules. 
Each lesson describes a common web vulnerability, and the basics behind how to exploit that vulnerability.
Each lesson also includes interactive exercises allowing you to practice exploiting the vulnerabilities.

---

## Prerequisites

You will need these servers setup before we start:

* WebGoat 8 instance
* Kali Linux instance (optional)

You can setup WebGoat 8 locally using vagrant, docker, or as a standalone Java application by following the [official documentation](https://github.com/WebGoat/WebGoat).
Alternatively you can use a paid service like [thehackerlab.co](https://thehackerlab.co) to set this up for you.

---

## Access WebGoat Lessons

1\. You can access your WebGoat instance by navigating to `138.68.55.203/WebGoat` in your browser.
Replace `138.68.55.203` with the IP address of your WebGoat server.

<img src="/assets/images/walkthroughs/webgoat-8/quickstart/3-login.PNG"> 

2\. Register as a new user to access the lessons.

<img src="/assets/images/walkthroughs/webgoat-8/quickstart/4-register.PNG">

3\. Each module listed in the navigation menu on the left includes lessons in incremental difficulty that teach you about the listed vulnerability.
Click on the numbered icons across the top of the page to progress through each lesson in the module.

<img src="/assets/images/walkthroughs/webgoat-8/quickstart/5-lessons.PNG">

---

## Login to Kali Linux (Optional)

1\. If you've chosen to setup the optional Kali Linux instance, [ssh](/essentials/ssh) into your Kali Linux server now.

```bash
# replace 178.128.77.75 with the IP address of your Kali Linux instance
ssh -p 2222 hacker-lab@104.248.66.22
```

[thehackerlab.co](https://thehackerlab.co) sets up the ssh service on port `2222` with access for the user `hacker-lab` using the ssh keys provided at setup by default.
Replace the user and port number with the correct values if you set it up manually.

---

## More Resources

* [Open Web Application Security Project (OWASP)](https://www.owasp.org/index.php/Main_Page) 
* [Github](https://github.com/WebGoat/WebGoat)
* [Docker](https://hub.docker.com/r/webgoat/webgoat-8.0)


