---
title: Injection Flaws SQL Injection (Introduction)
description: WebGoat 8 Injection Flaws - SQL Injection (Introduction)
extends: _layouts.documentation
section: content
---

# Injection Flaws - SQL Injection (Introduction)

## What is SQL?

The first exercise helps us learn how to **SELECT** from an SQL table. 
The following query should grab the information we're looking for in this exercise.

```mysql
# retrieve the department of the employee Bob Franco
SELECT department FROM employees WHERE first_name = 'Bob';
```

Let's breakdown the query:

* `SELECT department` - The `SELECT` command is used to specify what column of information we want. 
The available columns are given to us in the exercise description.
* `FROM employees` - The `FROM` command is used to specify what table we want to query. 
The name of the table is given to us in the exercise description.
* `WHERE first_name = 'Bob'` - The `WHERE` command is used to filter out any rows that don't match our given clause. 
In this case we only want rows where the value under the `first_name` column is `Bob`.
* `;` - The `;` is used to tell the query interpreter this is the end of our query.

>Hint: Checkout the MySQL's official documentation for the [SELECT Syntax](https://dev.mysql.com/doc/refman/8.0/en/select.html) to learn more.

---

## Data Manipulation Language (DML)

This next exercise helps us learn how to **UPDATE** an SQL table.
The following query should update the table as requested.

```mysql
# change the department of Tobi Barnett to 'Sales' 
UPDATE employees SET department = 'Sales' WHERE first_name = 'Tobi';
```

Let's breakdown the query:

* `UPDATE employees` - The `UPDATE` command is used to specify the table we want to update information for. 
The name of the table is given to us in the previous exercise description.
* `SET department = 'Sales'` - The `SET` command is used to specify what column we want to update, and what we want to change the value to. 
"Sales" is the value requested in the exercise description.
* `WHERE first_name = 'Tobi'` - The `WHERE` command is used to filter out any rows that don't match our given clause. 
In this case, we only want to update a row if the `first_name` is "Tobi".
* `;` - the `;` is used to tell the query interpreter this is the end of our query.

>Hint: Checkout the MySQL's official documentation for the [UPDATE Syntax](https://dev.mysql.com/doc/refman/8.0/en/update.html) to learn more.

---

## Data Definition Language (DDL)

This next exercise helps us learn how to **ALTER** an SQL table definition.
The following query should update the table definition as requested.

```mysql
# add the column "phone" (varchar(20)) to the table "employees
ALTER TABLE employees ADD COLUMN phone varchar(20);
```

Let's breakdown the query:

* `ALTER TABLE employees` - The `ALTER TABLE` command is used to specify the table definition we want to change.
* `ADD COLUMN phone` - The `ADD COLUMN` command is used to specify the name of a new column we want to add to the table definition.
* `varchar(20)` - The `varchar(20)` clause is used to specify what type of data the new column will store. In this case `varchar(20)` sets the data type to a string of up to 20 characters.
* `;` - The `;` is used to tell the query interpreter this is the end of our query.

>Hint: Checkout the MySQL's official documentation for the [ALTER TABLE Syntax](https://dev.mysql.com/doc/refman/8.0/en/alter-table.html) to learn more.

---

## Data Control Language (DCL)

This next exercise helps us learn how to **GRANT** access to alter table definitions.
The following query should grant the requested permissions.

```mysql
# grant the usergroup "UnauthorizedUser" the right to alter tables
GRANT ALTER TABLE TO UnauthorizedUser;
```

Let's breakdown the query:

* `GRANT ALTER TABLE` - The `GRANT` command is used to specify the type of permission we want to enable. 
In this case we're enabling the use of the `ALTER TABLE` command. 
* `TO UnauthorizedUser` - The `TO` command is used to specify who we want to enable the permissions for. 
In this case we're enable the `ALTER TABLE` permsissions for the `UnauthorizedUser` group. 
* `;` - The `;` is used to tell the query interpreter this is the end of our query.

>Hint: Checkout the MySQL's official documentation for the [GRANT Syntax](https://dev.mysql.com/doc/refman/8.0/en/grant.html) to learn more.

---

## Try It! String SQL Injection

This next exercise helps us learn how **String SQL injection** works.
Select the following values from the three drop down inputs to retrieve the requested information.

* `'`
* `or`
* `'1' = '`

So how does this work? The exercise definition shows us the query run on the server.
```mysql
SELECT * FROM user_data WHERE FIRST_NAME = 'John' and LAST_NAME = 'lastName';
```

Whatever we select from the dropdown input will be substituted in for the `lastName` variable before the query is run on the server.
Here is the query after substituting our input for the `lastName` variable.
```mysql
SELECT * FROM user_data WHERE first_name = 'John' AND last_name = '' OR '1' = '1';
```

Let's breakdown the final query:

* `SELECT * FROM user_data` - We learned from previous lessons that this means we want to select all columns from the `user_data` table.  
* `WHERE first_name = 'John'` - The first clause in the `WHERE` statement means we only want rows where the `first_name` value is `John`. 
* `AND last_name = '' OR '1' = '1'` - The second clause in the `WHERE` statement means we also only want rows with an empty last name, **OR** if `'1' = '1'`. 

The important point here is that `'1'` will always equal `'1'` for every row! 
So the database ends up retrieving the data for **ALL** rows instead of the one row that matches a specified user.

---

## Try It! Numeric SQL Injection

This next exercise helps us learn how **Numeric SQL injection** works.
Enter the following values in the text inputs to retrieve the requested information.

* Login_count - `1`
* User_Id - `1 OR 1=1`

So how does this work? The exercise definition shows us the query run on the server.
```mysql
SELECT * FROM user_data WHERE Login_Count = Login_Count AND USERID = User_ID;
```

Whatever we enter into the text input will be substituted in for the `Login_Count` and  `User_ID` variables before the query is run on the server.
Here is the query after substituting our input for the `Login_Count` and `User_ID` variables.
```mysql
SELECT * FROM user_data WHERE Login_Count = 1 and userid = 1 OR 1=1
```

Let's breakdown the final query:

* `SELECT * FROM user_data` - We learned from previous lessons that this means we want to select all columns from the `user_data` table.  
* `WHERE Login_Count = 1` - The first clause in the `WHERE` statement means we only want rows where the `Login_Count` value is `1`. 
* `AND userid = 1 OR 1 = 1` - The second clause in the `WHERE` statement means we also only want rows with an empty last name, **OR** if `1 = 1`.

The important point here is that `1` will always equal `1` for every row! 
So the database ends up retrieving the data for **ALL** rows instead of the one row that matches a specified user.

Notice that the String SQL injection exercise requires us to match the quotes, while this Numeric SQL injection exercise requires no quotes.
The difference is because we're taking advantage of a **string** type column in the first exercise, versus an **int** type column in this exercise.

>Hint: Checkout MySQL's official documentation on [data types](https://dev.mysql.com/doc/refman/8.0/en/data-types.html) to learn more.

---

## Compromising confidentiality with String SQL injection

This next exercise helps us learn how **String SQL injection** works.
Enter the following values in the text inputs to retrieve the requested information.

* Employee Name - `blah`
* Authentication TAN - `123' OR '1' = '1`

So how does this work? The exercise definition shows us the query run on the server.
 ```mysql
SELECT * FROM employees WHERE last_name = 'name' AND auth_tan = 'auth_tan';
```

Whatever we enter into the text input will be substituted in for the `name` and  `auth_tan` variables before the query is run on the server.
Here is the query after substituting our input for the `name` and `auth_tan` variables.
 ```mysql
SELECT * FROM employees WHERE last_name = 'blah' AND auth_tan = '123' OR '1' = '1';
```

Let's breakdown the final query:

* `SELECT * FROM employees` - We learned from previous lessons that this means we want to select all columns from the `employees` table.  
* `WHERE last_name = 'blah'` - The first clause in the `WHERE` statement means we only want rows where the `last_name` value is `blah`. 
* `AND auth_tan = '123' OR '1' = '1'` - The second clause in the `WHERE` statement means we also only want rows with `auth_tan` value of `'123'`, **OR** if `'1' = '1'`.

The important point here is that `'1'` will always equal `'1'` for every row! 
So the database ends up retrieving the data for **ALL** rows instead of the one row that matches a specified user.

---

## Compromising Integrity with Query chaining

This next exercise helps us learn how **Query Chaining** works.
Enter the following values in the text inputs to retrieve the requested information.

* Employee Name - `blah`
* Authentication TAN - `123'; UPDATE employees SET salary = 100000 WHERE auth_tan = '3SL99A`

So how does this work?
The previous exercise solution showed us what the employees table looks like.
This is how we know know the name of the column we want to update, and what value salary will be highest.
```mysql
USERID	FIRST_NAME	LAST_NAME	DEPARTMENT	SALARY	AUTH_TAN
32147	Paulina	    Travers	    Accounting	46000	P45JSI
34477	Abraham	    Holman	    Development	50000	UU2ALK
37648	John	    Smith	    Marketing	64350	3SL99A
89762	Tobi	    Barnett	    Development	77000	TA9LL1
96134	Bob	        Franco	    Marketing	83700	LO9S2V
```

The current exercise definition also shows us the query run on the server.
```mysql
SELECT * FROM employees WHERE last_name = 'name' AND auth_tan = 'auth_tan';
```

Whatever we enter into the text input will be substituted in for the `name` and  `auth_tan` variables before the query is run on the server.
Here is the query after substituting our input for the `name` and `auth_tan` variables.
 ```mysql
SELECT * FROM employees WHERE last_name = 'blah' AND auth_tan = 'auth_tan123'; UPDATE employees SET salary = 100000 WHERE auth_tan = '3SL99A';
```

Let's breakdown the final query:

* `SELECT * FROM employees WHERE last_name = 'blah' AND auth_tan = '123';` - This query will return nothing since there is no row with `last_name = 'blah' AND auth_tan = '123'`.
But the important thing to see is we've successfully added a `;` to the end of this statement. 
Which allows us to start a whole new statement.
* `UPDATE employees SET salary = 100000 WHERE auth_tan = '3SL99A'` - And we'll exploit that by chaining an `UPDATE` statement that sets our `salary` to `100000`  

---

## Compromising Availability

This next exercise helps us learn how the **DROP** command works.
Enter the following values in the text inputs to retrieve the requested information.

* Action contains - `blah'; DROP table access_logs; --`

So how does this work?
We can guess that the query run on the server looks something like this
```mysql
SELECT * FROM access_logs WHERE action LIKE '%action_contains%';
```

We don't need to know the actual names of the columns since we aren't planning to alter or query any specific column.
Here is the query after substituting our input for the `action_contains` column

```mysql
SELECT * FROM access_logs WHERE action LIKE '%blah'; DROP table access_logs; --%';
``` 

Let's breakdown the final query:

* `SELECT * FROM access_logs WHERE action LIKE '%blah';` - Just like the previous exercise, this query will probably return nothing, but that isn't important.
The important part is the `;` to end the statement and allow us to chain a second statement to run.
* `DROP table access_logs;`- The `DROP` command allows us to delete the entire `access_logs` table.
Notice we've added another `;` to end the command
* ` --%';`- The `--` syntax tells MySQL that anything following is a comment. 
This allows us to tell the server to ignore the characters after our `DROP` command so we don't run into any syntax errors.