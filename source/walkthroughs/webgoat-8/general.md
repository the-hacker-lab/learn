---
title: General
description: WebGoat 8 General
extends: _layouts.documentation
section: content
---

# General

## HTTP Basics

### The Quiz

1\. The quiz asks for the type of HTTP Request used, as well as the value of a magic number. 

<img src="/assets/images/walkthroughs/webgoat-8/http-basics/1-the-quiz.PNG">

2\. Let's start by opening up the **Browser Developer Tools** with the keyboard shortcut `CTRL + SHIFT + I`.
Open the **Network** tab so we can view the network request made when we submit the form.

>Hint: The developer console is part of a set of developer tools built right into most popular web browsers.
>We can use it to inspect what data the web page is sending back to the browser.
>You can learn more about what the developer tools can be used for [here](/essentials/browser-developer-tools).

<img src="/assets/images/walkthroughs/webgoat-8/http-basics/2-developer-console.PNG">

3\. Now let's fill out the quiz form with a random guess and submit the form.
The purpose of this test submission is so we can inspect what the form sends back to the server using the **Firefox Developer Tools**. 

4\. Watch the **Network** panel as you click "submit" to see a new request to the `attack2` recorded.
Let's inspect the data sent in that request by clicking on the row for the `attack2` request in the **Network** tab, then clicking on the **Headers** sub tab.
The **Headers** tab is where we can view meta data like where the request was sent, what type of request was sent, and what response code was returned to the request.

>Hint: Checkout Firefox's official documentation for the [network tab](https://developer.mozilla.org/en-US/docs/Tools/Network_Monitor) to learn more.

<img src="/assets/images/walkthroughs/webgoat-8/http-basics/3-test.PNG">

5\. Under the **Headers** sub tab, we can see that the `Request method` value is `POST`.

Now let's click over to the **Params** sub tab. This is where can see what parameters were passed back to the web server.
Here we can see the form input we submitted, as well as an additional parameter called `magic_num`.

<img src="/assets/images/walkthroughs/webgoat-8/http-basics/5-params.PNG">

6\. Let's submit the information we've uncovered and view the success message.

<img src="/assets/images/walkthroughs/webgoat-8/http-basics/7-success.PNG">

