---
title: Injection Flaws Sql Injection
description: WebGoat 8 Injection Flaws Advanced
extends: _layouts.documentation
section: content
---

# Injection Flaws - SQL Injection (Advanced)

## Try It! Pulling data from other tables

The exercise description gives us the table definition for the `users` table:

* USERID - integer
* FIRST_NAME - string
* LAST_NAME - string
* CC_NUMBER - string
* CC_TYPE - string
* COOKIE - string
* LOGIN COUNT - integer

And the `user_system_data` table:

* userid - integer
* user_name - string
* password - string
* cookie - string


A `UNION` operation requires:

1. the same number of columns from both result sets
2. matching column types across both result sets

The following query meets these requirements by repeating certain rows from the `user_system_data` table to match the number of columns and column types from the `users` table

```mysql
select * from users 
where FIRST_NAME = 'Smith' 
union all 
select userid, user_name, user_name, user_name, user_name, password, userid 
from user_system_data;--
```

WebGoat is probably running this query behind the scenes of the user form:

```mysql
"select * from users where FIRST_NAME = '" name + '"
``` 

You can achieve the successful `UNION` query from step 2 by submitting the following value into the name field of the web form:

```mysql
Smith' union all select userid, user_name, user_name, user_name, user_name, password, userid from user_system_data;--
```

<img src="/assets/images/walkthroughs/webgoat-8/injection-flaws/sql-injection-advanced/4-union.PNG">

You can see from the result that dave's password is `passW0rD`.

>Hint: Checkout the MySQL's official documentation for the [UNION Syntax](https://dev.mysql.com/doc/refman/8.0/en/union.html) to learn more.

---

## Goal: Can you login as Tom?

1\. Navigate to the register form and register a new user "testuser"

<img src="/assets/images/walkthroughs/webgoat-8/injection-flaws/sql-injection-advanced/1-register.PNG">

2\. Try the following SQL snippet in the `username` field: 
```mysql
testuser' AND '1'='1
```

Notice the following error message: `User testuser' AND '1'='1 already exists please try to register with a different username.`
<img src="/assets/images/walkthroughs/webgoat-8/injection-flaws/sql-injection-advanced/2-true-statement.PNG">

3\. Try the following SQL snippet in the `username` field:
```mysql
testuser' AND '1'='2
```

Notice the following success message:`User testuser' AND '1'='2 created, please proceed to the login page.`
<img src="/assets/images/walkthroughs/webgoat-8/injection-flaws/sql-injection-advanced/3-false-statement.PNG">

The first SQL query you tried evaluated to `true` and returned an error message to the browser. 
The second SQL query you tried evaluated to `false` and returned a success message to the browser.
You can use this feedback to string together true/false questions that will help build Tom's password.

4\. For example, let's ask if the first character of Tom's password is the character "a".
Submit this as the username `tom'+and+substring(password,1,1)='a`

Notice we get the message that means the statement we asked evaluates to `false`
<img src="/assets/images/walkthroughs/webgoat-8/injection-flaws/sql-injection-advanced/4-first-character-fail.PNG">

5\. Let's keep incrementing the character until we get the message that means we've got a `true` statement
Looks like the first character of the password is "t"
<img src="/assets/images/walkthroughs/webgoat-8/injection-flaws/sql-injection-advanced/5-first-character-success.PNG">

6\. Now let's use the same strategy on the second character of the password by incrementing the substring position param.
We eventually see that the second character is "h".
<img src="/assets/images/walkthroughs/webgoat-8/injection-flaws/sql-injection-advanced/6-second-character-success.PNG">

7\. Let's automate this procedure to enumerate the remaining password characters. 
Download, install, and run the [Burp Suite Community Edition](https://portswigger.net/burp).
Create a temp project by clicking through all the default settings of the temp project setup wizard until you reach the project window

8\. We're going to run an `intruder` attack. 
Burp Intruder is a powerful tool for performing automated customized attacks against web applications. It is extremely flexible and configurable, and can be used to automate all kinds of tasks that arise when testing applications.
Select the `intruder` tab. Click on the `positions` sub tab and set the following settings:

<img src="/assets/images/walkthroughs/webgoat-8/injection-flaws/sql-injection-advanced/8-positions.PNG">

You'll need to replace the `Cookie` value by copying the Cookie value out of your request header through the developer console (ctrl + shift + i)

9\. Set the target by clicking on the `target` sub tab and entering the IP address of your WebGoat instance.

<img src="/assets/images/walkthroughs/webgoat-8/injection-flaws/sql-injection-advanced/9-target.PNG">

10\. Click on the `payload` sub tab and set the following settings:

<img src="/assets/images/walkthroughs/webgoat-8/injection-flaws/sql-injection-advanced/10-payload.PNG">

11\. Click on "start attack" and watch as Burp cycles through all of the payload options.
Sort the results by length and see that it got a different result length for the "t" payload which means this is how you'll identify a `true` return value

<img src="/assets/images/walkthroughs/webgoat-8/injection-flaws/sql-injection-advanced/11-results.PNG">

12\. Continue this process by incrementing the substring position parameter until you've found the entire password: `thisisasecretfortomonly`

<img src="/assets/images/walkthroughs/webgoat-8/injection-flaws/sql-injection-advanced/12-success.PNG">