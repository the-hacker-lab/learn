@extends('_layouts.master')

@section('body')
<section class="container max-w-2xl mx-auto px-6 py-10 md:py-12">
    <div class="flex flex-col-reverse mb-10 lg:flex-row lg:mb-24">
        <div class="mt-8">
            <h1 id="intro-docs-template">{{ $page->siteName }}</h1>

            <h2 id="intro-powered-by-jigsaw" class="font-light mt-4">{{ $page->siteDescription }}</h2>

            <p class="text-lg">Designed specifically for beginners, you'll learn the fundamentals of NetSec. <br/>From using your first text editor to hacking your first server.</p>

            <div class="flex my-10">
                <a href="/getting-started" title="{{ $page->siteName }} getting started" class="bg-blue hover:bg-blue-dark font-normal text-white hover:text-white rounded mr-4 py-2 px-6">Get Started</a>

                <a href="https://gitlab.com/the-hacker-lab/learn" title="Contribute" class="bg-grey-light hover:bg-grey-dark text-blue-darkest font-normal hover:text-white rounded py-2 px-6">Contribute</a>
            </div>
        </div>

        <img src="/assets/img/logo-large.svg" alt="{{ $page->siteName }} large logo" class="mx-auto mb-6 lg:mb-0 ">
    </div>

    <hr class="block my-8 border lg:hidden">

    <div class="md:flex -mx-2 -mx-4">
        <div class="mb-8 mx-3 px-2 md:w-1/3">
            <img src="/assets/img/icon-window.svg" class="h-12 w-12" alt="window icon">

            <h3 id="intro-mix" class="text-2xl text-blue-darkest mb-0">CTF walkthrough guides</h3>

            <p>Use our step by step guides to practice hacking intentionally vulnerable servers like WebGoat, DVWA, or Metasploitable.</p>
        </div>

        <div class="mb-8 mx-3 px-2 md:w-1/3">
            <img src="/assets/img/icon-terminal.svg" class="h-12 w-12" alt="terminal icon">

            <h3 id="intro-markdown" class="text-2xl text-blue-darkest mb-0">Join our community</h3>

            <p>Join our <a target="_blank" href="https://discord.gg/8bBQrQ7">discord server</a> to get help, request features, or just hang out.</p>
        </div>

        <div class="mx-3 px-2 md:w-1/3">
            <img src="/assets/img/icon-stack.svg" class="h-12 w-12" alt="stack icon">

            <h3 id="intro-mix" class="text-2xl text-blue-darkest mb-0">Contribute to open source</h3>

            <p>This documentation is open source under the MIT license. Checkout our <a target="_blank" href="/contribute">contribution guide</a> to help improve this documentation.</p>
        </div>
    </div>
</section>
@endsection
