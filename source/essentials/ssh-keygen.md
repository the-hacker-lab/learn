---
title: ssh-keygen
description: ssh-keygen
extends: _layouts.documentation
section: content
---

# What is ssh-keygen

`ssh-keygen` is a linux utility that generates ssh key pairs used to securely ssh into servers.

## How to Generate SSH Keys

The following steps will show you how to generate ssh keys you can use to [ssh](/essentials/ssh) into a remote host.

1\. Run the `ssh-keygen` command from your command line.
The wizard will prompt you for:

* a key name
* a password

This is what the entire process looks like:

```bash
$ ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/home/wagena/.ssh/id_rsa): hacker-lab
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in hacker-lab.
Your public key has been saved in hacker-lab.pub.
The key fingerprint is:
SHA256:64eV38IxNBb+qU2B1mIa7SBHWeRoSWS1LoHoX5rAGX0 wagena@496654e01693
The key's randomart image is:
+---[RSA 2048]----+
|         .+=+    |
|       o +o+..   |
|      o o.Eoo+   |
|     o o.o+o@ o  |
|      = Soo@.+ o |
|       o =+.+ +  |
|        =o o B   |
|       .. . = o  |
|        ..   .   |
+----[SHA256]-----+
```

And that's about it! `ssh-keygen` uses default values to make the process as easy as possible.

> Tip: You can run the `man ssh-keygen` command to see what kind of parameters you can use to customize things a bit more.

But let's break the defaults down a bit in the following steps:

2\. Run the `ssh-keygen` command. This is where you would specify any custom parameters. 
But we're just using the default settings here:

* 2048 bits
* RSA type 

```bash
$ ssh-keygen
Generating public/private rsa key pair.
```

3\. The first prompt is for a filename to save your new keys under.
You can hit enter to use the default `id_rsa`. 
This would save your private key at `~/.ssh/id_rsa` and your public key at `~/.ssh/id_rsa.pub`.

> Tip: your shell will expand `~` to your home directory. So if your username is `wesley`, your shell will interpret `~/.ssh/id_rsa` as `/home/wesley/.ssh/id_rsa`.

Here I entered `hacker-lab` as my input instead of using the default.
This would save my private key at `~/.ssh/hacker-lab` and your public key at `~/.ssh/hacker-lab.pub`.
```bash
Enter file in which to save the key (/home/wagena/.ssh/id_rsa): hacker-lab
```

4\. The next prompt is for a password.
You'll use this password whenever you use these keys to `ssh` into a server.
Hitting `enter` without a password will set no password on your keys. 
This is **not** recommended since someone else will be able to use your keys without a password if they ever get a hold of them.
```bash
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
```

5\. After entering a password it's just giving us some summary information.
The first bit we already know, our private key filename is `hacker-lab`, and our public key filename is `hacker-lab.pub`
```bash
Your identification has been saved in hacker-lab.
Your public key has been saved in hacker-lab.pub.
```

6\. The last piece of information is a fingerprint of your public key.
This is just used as an easy way to identify your key instead of trying to read the entire text of the key.
The randomart is an even easier way to identify or compare your key.
```bash
The key fingerprint is:
SHA256:64eV38IxNBb+qU2B1mIa7SBHWeRoSWS1LoHoX5rAGX0 wagena@496654e01693
The key's randomart image is:
+---[RSA 2048]----+
|         .+=+    |
|       o +o+..   |
|      o o.Eoo+   |
|     o o.o+o@ o  |
|      = Soo@.+ o |
|       o =+.+ +  |
|        =o o B   |
|       .. . = o  |
|        ..   .   |
+----[SHA256]-----+
```

## Next Steps

Checkout the man pages from your command line (`man ssh-keygen`) or [online](https://linux.die.net/man/1/ssh-keygen) to learn more about `ssh-keygen`.
