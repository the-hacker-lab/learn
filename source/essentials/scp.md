---
title: scp
description: scp
extends: _layouts.documentation
section: content
---

# What is SCP

`scp` is a linux utility that allows you to securely copy files between local and remote hosts.

## How to SCP

1\. The following command will copy the `passwords.txt` file from your home directory to the home directory of the `wes` user at a server with an IP address of `123.123.123.123`

```bash
scp ~/passwords.txt wes@123.123.123.123:~/
```

It assumes you're using the default ssh key at ~/.ssh/id_rsa

> Tip: your shell will expand `~` to your home directory. So if your username is `wesley`, your shell will interpret `~/.ssh/id_rsa` as `/home/wesley/.ssh/id_rsa`.

2\. The following command will copy and entire directory from your home directory to the home directory of the `wes` user at a server with an IP address of `123.123.123.123`
 
 ```bash
scp -r ~/secret_info wes@123.123.123.123:~/secret_info
```

Notice the addition of the `-r` argument.
Again, this assumes you're using the default ssh key at `~/.ssh/id_rsa`.

3\. The following command will copy the `passwords.txt` file from `./my-directory` to `/another_directory` of the remote server.
But this time we're using the `-i` option to specify a custom private ssh key.
```bash
scp -i ~/.ssh/my_custom_private_key ./my-directory/passwords.txt wesley@123.123.123.123:/another_directory
```

## Next Steps

Checkout the man pages from your command line (`man scp`) or [online](https://linux.die.net/man/1/scp) to learn more about `scp`.