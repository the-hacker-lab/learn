---
title: browser developer tools
description: browser developer tools
extends: _layouts.documentation
section: content
---

# Browser Developer Tools


Browser developer tools are developer tools built right into the browser. The rest of this tutorial will reference the Chrome Browser developer tools in examples, but every browser has it's own set of developer tools and the examples should transfer to each browser pretty easily.


You can open the developer tools in the chrome browser with the keyboard shortcut `CTRL+SHIFT+I` (windows) or `CMD+SHIFT+C` (mac)


[screenshot]


There are several panels available in the developer tools. In this tutorial, we'll cover two of the panels that might be useful for beginners:


* Network Panel
* Elements Panel


> Hint: Visit the [Chrome DevTools](https://developers.google.com/web/tools/chrome-devtools/) official documentation if you want to explore these panels further, or dive into the other available developer panels.


## Network Panel


The **Network Panel** shows you all of the requests that the browser makes to other web servers on the internet. Try opening the **Network Panel** and refreshing the page. You'll see that a single web page might require 10s or even 100s of separate requests to finish building the web page. Requests might include:


* the actual page you're trying to view (.com or other tld extension)
* javascript assets (.js) that the page requires
* style sheets (.css)  that style the page
* google fonts
* images, logos, or icons that are served from a CDN (not embedded on the page)
* requests to an Ad server to show you Ads
* separate Ajax or api requests to retrieve information to display on the page
* any push notifications the page is receiving 
* plenty of other possibilities


One feature that will be useful for student hackers is the ability to see the request and response data for any request made. Try clicking on one of the requests in the **Network Panel** and you'll see a pane open with these tabs:


* Headers
* Preview
* Response
* Cookies
* Timing


[screenshot]


Click on the **Headers** tab to see the data sent to the server during a request. For example, if you view the request made when logging into a website, you should see your username and password being passed to the server.


Click on the **Response** tab to see the data returned from the server during a request. For example, the same login server might send back a success response and redirect you to the home page if your login credentials were correct.


Use the **Filter** text box to filter the requests by name.


> Hint: Visit the [Network Panel](https://developers.google.com/web/tools/chrome-devtools/network/) official documentation to learn more about what you can do with the **Network Panel**.


## Elements Panel


The **Elements Panel** shows you all of the html elements on a page. Try right clicking on some text on the page and selecting the **Inspect** option. You'll see the html used to render the text you're inspecting highlighted in the **Elements Panel**


>Hint: Visit the [Elements Panel](https://developers.google.com/web/tools/chrome-devtools/dom/) official documentation to learn more about what you can do with the **Elements Panel**


## What's Next


The examples in this tutorial use the Chrome Developer Tools. Here are some links to developer tools of other major browsers if you aren't using Chrome.


* [Chrome DevTools](https://developers.google.com/web/tools/chrome-devtools/)
* [Firefox Developer Tools](https://developer.mozilla.org/en-US/docs/Tools)
* [Safari Developer Tools](https://developer.apple.com/safari/tools/)