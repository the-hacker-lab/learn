---
title: ssh
description: ssh
extends: _layouts.documentation
section: content
---

# What is ssh

`ssh` is a linux utility that allows you to securely connect to a server and run a shell on that server remotely from your local command line.

## How to ssh into a server

The following steps will show you how to ssh into a remote host using your [ssh keys](/linux-basics/ssh-keygen).

1\. The following is the most basic way to use `ssh`.

* **hostname** - can be an IP Address, Fully Qualified Domain Name (FQDN), or Hostname.
* **user** - is the user that has your public key installed.

```bash
$ ssh user@hostname
```

This command will give you a shell as the user `user`, on server with a hostname of `hostname`.
It will use the default private ssh key at `~/.ssh/id_rsa` to SSH into the server.
It will also prompt you for your password if you set one during the [ssh-keygen](/essentials/ssh-keygen) setup process.

## Optional flags

1\. If your SSH keys are named something other than the default (`~/.ssh/id_rsa`), you can specify which keys to use with the `-i` flag.

```bash
$ ssh -i ~/.ssh/privatekeyfile user@hostname
```

2\. If a server is running ssh on a port other than default (22), then you can specify the port with the `-p` flag.

```bash
# connect to a server that is running ssh service on the non standard 2222 port.
$ ssh -p 2222 user@hostname
```

## How to view your public ssh key

Some services like [github.com](https://github.com) or [thehackerlab.co](https://thehackerlab.co) allow you to add your public key as text input.

You can use the `cat` command to view your public ssh key and copy the text.

```bash
$ cat ~/.ssh/id_rsa.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDiFW2rXdIQh6ywanF3ZpaKZmzZkseHVu3KprzTvgWiKNGeI3buXI2prRUhPMY0IR0r3pGuoDPwLiQ/HaZywIS69QYaINX6P2hzTp46uwK4gDG/AFqIZxhA1cIeutVmKzTKTpYEBzgnYGO8+OWmLOKaotLENK3f0BuLG23UnRk2Ns+DMe5fwQms16HYW2aINlJ4gExW28SmqnggWbZeSAJ+Ny/ukBTTWBPkLu4Bi4/ouohyD3MipSly3+N/oLxcEvabiocNEt6A7wq5AHT5QLhn0krussE96FhN2weIx3VJgmwqFd0CSTnDNuSirZXjesqIAuxImKcRUfczyC/60NFp wagena@8077056928df
```

Copy and paste the entire string to add your public key to a server.

Remember, if you named you're keys something other than the default `id_rsa`, switch out the `id_rsa.pub` file with your custom named public key in the `cat` command.

## Next Steps

Checkout the man pages from your command line (`man ssh`) or [online](https://linux.die.net/man/1/ssh) to learn more about `ssh`.