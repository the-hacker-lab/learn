---
title: Getting Started
description: Getting started with Jigsaw's docs starter template is as easy as 1, 2, 3.
extends: _layouts.documentation
section: content
---

# Getting Started


## What is this?


[learn.thehackerlab.co](https://learn.thehackerlab.co) is a documentation site that contains step by step guides to walk you through popular open source Capture the Flag (CTF) challenges like [WebGoat](https://github.com/WebGoat/WebGoat), [DVWA](https://github.com/ethicalhack3r/DVWA), or [Metasploitable](https://github.com/rapid7/metasploitable3). 

It also contains reference material for all the tools you'll be using during the CTF challenges. 
And explanations of basic security concepts and the underlying technology used. 

So beginners can build a solid foundation when starting from scratch.

## Where do I start?

First you should setup a lab environment that includes:

1\. **An instance of Kali Linux** - This is a linux distribution that comes pre-loaded with many common security tools. It is perfect to use as our pen testing machine.

2\. **A vulnerable server** - This will be an intentionally vulnerable server that we can attack with our Kali Linux instance. 

There are several open source options including:


* WebGoat
* Damn Vulnerable Web App
* Metasploitable Ubuntu
* Metasploitable Windows

Once you have your virtual lab setup, navigate to our walkthrough guide in the left side menu to start hacking!

>Tip: If you want to skip the lab setup and get straight to hacking, signup at [thehackerlab.co](https://thehackerlab.co) to create lab environments in the cloud with a single click.

## Anything else?

This documentation is completely open source licensed under MIT. Feel free to [contribute](/contribute).