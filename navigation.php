<?php

return [
    'Getting Started' => [
        'url' => 'getting-started',
    ],
    'WebGoat 8 Walkthroughs' => [
        'children' => [
            'Quickstart' => 'walkthroughs/webgoat-8/quickstart',
            'General' => '/walkthroughs/webgoat-8/general',
            'Injection Flaws - SQL Injection (Introduction)' => '/walkthroughs/webgoat-8/injection-flaws-sql-injection-introduction',
            'Injection Flaws - SQL Injection (Advanced)' => '/walkthroughs/webgoat-8/injection-flaws-sql-injection-advanced'
        ],
    ],
    'Damn Vulnerable Web App (DVWA) Walkthroughs' => [
        'children' => [
            'Quickstart' => '/walkthroughs/damn-vulnerable-web-app/quickstart',
            'Brute Force' => '/walkthroughs/damn-vulnerable-web-app/brute-force',
            'Command Injection' => '/walkthroughs/damn-vulnerable-web-app/command-injection',
            'Cross Site Request Forgery (CSRF)' => '/walkthroughs/damn-vulnerable-web-app/cross-site-request-forgery'
        ],
    ],
    'Metasploitable 3 Ubuntu Walkthroughs' => [
        'children' => [
            'Quickstart' => '/walkthroughs/metasploitable-3-ubuntu/quickstart',
            'Ace of Clubs' => '/walkthroughs/metasploitable-3-ubuntu/ace-of-clubs',
            '8 of Clubs' => '/walkthroughs/metasploitable-3-ubuntu/8-of-clubs',
            '5 of Hearts' => '/walkthroughs/metasploitable-3-ubuntu/5-of-hearts'
        ],
    ],
    'Metasploitable 3 Windows Walkthroughs' => [
        'children' => [
            'Quickstart' => '/walkthroughs/metasploitable-3-windows/quickstart',
            'King of Diamonds' => '/walkthroughs/metasploitable-3-windows/king-of-diamonds',
            'Ace of Hearts' => '/walkthroughs/metasploitable-3-windows/ace-of-hearts',
            'Jack of Hearts' => '/walkthroughs/metasploitable-3-windows/jack-of-hearts'
        ],
    ],
    'Essentials' => [
        'children' => [
            'scp' => '/essentials/scp',
            'ssh' => '/essentials/ssh',
            'ssh-keygen' => '/essentials/ssh-keygen',
            'Browser Developer Tools' => '/essentials/browser-developer-tools'
        ],
    ],
    'Contribute' => [
        'url' => 'contribute',
    ],
];
