[View a preview of the docs template.](http://jigsaw-docs-staging.tighten.co/)


## Building Your Site

Now that you’ve edited your configuration variables and know how to customize your styles and content, let’s build the site.

```bash
# build static files with Jigsaw
./vendor/bin/jigsaw build

# compile assets with Laravel Mix
# options: dev, staging, production
npm run dev

in learn dir, npm run watch
```
