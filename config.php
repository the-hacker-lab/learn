<?php

return [
    'baseUrl' => '',
    'production' => true,
    'siteName' => 'Hacker Lab Learn',
    'siteDescription' => 'We all start somewhere. This should be your first stop when it comes to NetSec. ',

    // Algolia DocSearch credentials
    'docsearchApiKey' => '',
    'docsearchIndexName' => '',

    // navigation menu
    'navigation' => require_once('navigation.php'),

    // helpers
    'isActive' => function ($page, $path) {
        return ends_with(trimPath($page->getPath()), trimPath($path));
    },
    'isActiveParent' => function ($page, $menuItem) {
        if (is_object($menuItem) && $menuItem->children) {
            return $menuItem->children->contains(function ($child) use ($page) {
                return trimPath($page->getPath()) == trimPath($child);
            });
        }
    },
    'url' => function ($page, $path) {
        return starts_with($path, 'http') ? $path : '/' . trimPath($path);
    },
    'collections' => [
        'walkthroughs' => [
            "path" => 'walkthroughs',
        ]
    ]
];
